import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainLayoutComponent } from '@app/layout';
import { AuthenticationGuard, UnAuthenticationGuard, UserProfileExistGuard } from '@app/auth';
import { HomePageComponent } from './home-page/home-page.component';
import { DashboardLayoutComponent } from './layout/dashboard-layout/dashboard-layout.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        canActivate: [UnAuthenticationGuard],
        component: HomePageComponent
      },
      {
        path: 'signin',
        canActivate: [UnAuthenticationGuard],
        loadChildren: 'src/app/signin/signin.module#SigninModule'

      },
      {
        path: 'signup',
        canActivate: [UnAuthenticationGuard],
        loadChildren: 'src/app/signup/signup.module#SignupModule'

      }
    ]
  },
  {
    path: 'dashboard',
    component: DashboardLayoutComponent,
    canActivate: [AuthenticationGuard, UserProfileExistGuard],
    children: [
      {
        path: '',
        loadChildren: 'src/app/dashboard/overview/overview.module#OverviewModule'

      },
      {
        path: 'settings',
        loadChildren: 'src/app/dashboard/user-settings/user-settings.module#UserSettingsModule'
      },
      {
        path: 'profile',
        loadChildren: 'src/app/dashboard/user-profile/user-profile.module#UserProfileModule'
      },
      {
        path: 'applications',
        loadChildren: 'src/app/dashboard/user-applications/user-applications.module#UserApplicationsModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
