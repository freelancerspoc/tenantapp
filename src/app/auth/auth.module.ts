import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AuthInterceptor } from './interceptors/auth-interceptor';

import { NotificationService } from '@app/ui/notification';
import { SERVICES } from './services';
import { GUARDS } from './guards';
import { AuthEffects, SignupEffects, authReducer } from './store';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('auth', authReducer),
    EffectsModule.forFeature([AuthEffects, SignupEffects])
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    NotificationService,
    ...SERVICES,
    ...GUARDS
  ]
})
export class AuthModule {
}
