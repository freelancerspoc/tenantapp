import { Injectable } from '@angular/core';
import { Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { Logger } from '@app/core/logger';
import { AuthService } from '../services';
import { CONFIG } from '../auth.config';

const log = new Logger('AuthenticationGuard');

@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.authService.isAuthenticated()) {
      return true;
    }

    log.debug('Not authenticated, redirecting...');
    const url = state.url.substr(1, state.url.length) || '/';
    this.router.navigate([CONFIG.LOGIN.PATH], {
      queryParams: { returnUrl: url }
    });
    return false;
  }
}
