import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from '../services';
import { CONFIG } from '../auth.config';

@Injectable()
export class AuthorizationGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const expectedRoles: Array<string> = next.data.expectedRoles;
    const loggedInUser = this.authService.getloggedInUser();

    if (
      !loggedInUser ||
      !this.authService.isAuthenticated() ||
      !expectedRoles.includes(loggedInUser.role)
    ) {
      const url = state.url.substr(1, state.url.length) || '/';
      this.router.navigate([CONFIG.LOGIN.PATH], { queryParams: { returnUrl: url } });

      return false;
    }

    return true;
  }
}
