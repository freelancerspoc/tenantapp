import { AuthenticationGuard } from './authentication.guard';
import { UnAuthenticationGuard } from './un-authentication.guard';
import { AuthorizationGuard } from './authorization.guard';
import { UserProfileExistGuard } from './user-profile-exist.guard';

export {
  AuthenticationGuard,
  UnAuthenticationGuard,
  AuthorizationGuard,
  UserProfileExistGuard
};

export const GUARDS = [
  AuthenticationGuard,
  UnAuthenticationGuard,
  AuthorizationGuard,
  UserProfileExistGuard
];
