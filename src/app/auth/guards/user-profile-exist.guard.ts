import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, tap, take, filter } from 'rxjs/operators';

import { AuthState, selectCurrentUser, GetCurrentUser } from '../store';

@Injectable()
export class UserProfileExistGuard implements CanActivate {
  constructor(private store: Store<AuthState>) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.hasUserProfileInStore();
  }

  hasUserProfileInStore(): Observable<boolean> {
    return this.store.pipe(
      select(selectCurrentUser),
      map((user) => !!user),
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(new GetCurrentUser());
        }
      }),
      filter(loaded => loaded),
      take(1)
    );
  }

}
