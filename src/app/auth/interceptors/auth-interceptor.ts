import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@app/env';
import { AuthService } from '../services';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = this.authService.getAuthToken();
    let authorizationValue = '';
    if (authToken) {
      authorizationValue = `Bearer ${authToken.accessToken}`;
    }
    let authReq: any;

    const needToken = environment.autoAuthorizedUris.some(regex => {
      return regex.test(req.url);
    });

    if (needToken) {
      authReq = req.clone({
        headers: req.headers.set('Authorization', authorizationValue)
      });
    } else {
      return next.handle(req);
    }

    return next.handle(authReq);
  }

}
