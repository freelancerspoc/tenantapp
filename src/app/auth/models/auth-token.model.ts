export class AuthToken {
  expiresAt: any;

  constructor(
    public accessToken: string,
    public expiresIn: number,
    public refreshToken?: string,
    public jwt?: string,
    public idToken?: string,
    public tokenType: string = 'Bearer') {
    this.expiresAt = (this.expiresIn * 1000) + new Date().getTime();
  }

  public static isValid(authToken: AuthToken) {
    return (authToken.accessToken && !this.isExpired(authToken));
  }

  public static isExpired(authToken: AuthToken): boolean {
    return new Date().getTime() > authToken.expiresAt;
  }
}
