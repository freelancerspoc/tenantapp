export interface CreateDefaultAccountModel {
  uid: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
}
