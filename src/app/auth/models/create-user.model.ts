export interface CreateUserModel {
    username: string;
    password: string;
    email?: string;
    phoneNumber?: string;
}
