export enum UserRoles {
  SuperAdmin = 'SuperAdmin',
  Administrator = 'Administrator',
  Editor = 'Editor',
  Author = 'Author',
  Contributor = 'Contributor',
  Subscriber = 'Subscriber',
}
