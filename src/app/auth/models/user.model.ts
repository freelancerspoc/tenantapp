export interface User {
  id: string;
  address1?: string;
  address2?: string;
  avatar?: string;
  city?: string;
  country?: string;
  email?: string;
  emailVerified?: boolean;
  firstName?: string;
  lastName?: string;
  password?: string;
  payment?: Payment;
  phoneNumber?: string;
  phoneVerified?: boolean;
  postalCode?: string;
  state?: string;
  street?: string;
  username: string;
  role?: string;
  location: string;
  bio: string;
  angelList: string;
  facebook: string;
  twitter: string;
  linkedIn: string;
  website: string;
  hideMyProfile: string;
}

export interface Payment {
  paypal: Paypal;
}

export interface Paypal {
  email: string;
  phoneNumber: string;
}
