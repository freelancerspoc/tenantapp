import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment as env } from '@app/env';
import { CreateDefaultAccountModel, UpdateUserAuthenticationModel } from '../models';

@Injectable()
export class AccountService {

  constructor(private http: HttpClient) { }

  checkExisting(phoneNumber: any): Observable<any> {
    return this.http.get(`${env.rootApi}/tenants/check-existing/${phoneNumber}`);
  }

  createDefaultAccount(createDefaultAccountModel: CreateDefaultAccountModel): Observable<any> {
    return this.http.post(`${env.rootApi}/tenants`, createDefaultAccountModel);
  }

  updateUserAuthentication(updateUserAuthenticationModel: UpdateUserAuthenticationModel): Observable<any> {
    return this.http.post(`${env.rootApi}/tenants/update-authentication`, updateUserAuthenticationModel);
  }

  updateTenant(data: any): Observable<any> {
    return this.http.put(`${env.rootApi}/tenants/${data.uid}`, data);
  }

  setUseranmeAndPassword(data: any): Observable<any> {
    return this.http.put(`${env.rootApi}/tenants/${data.uid}/setUsernameAndPassword`, data);
  }

  login(data: any): Observable<any> {
    return this.http.post(`${env.rootApi}/tenants/login`, data);
  }

  isInBase(uid: string): Observable<any> {
    return this.http.get(`${env.rootApi}/tenants/${uid}`);
  }
}
