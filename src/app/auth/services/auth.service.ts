import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, timer, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { environment as env } from '@app/env';
import { HttpResponseData } from '@app/core/http';
import { LocalStorageService } from '@app/core/services';

import { AuthToken, User, Credential, CreateUserModel } from '../models';

@Injectable()
export class AuthService {
  private refreshTokenSubscription: Subscription;

  constructor(
    private http: HttpClient,
    private storage: LocalStorageService) {
  }

  public isAuthenticated(): Observable<boolean> | Promise<boolean> | boolean {
    const authToken: AuthToken = this.getAuthToken();
    return (!!authToken && AuthToken.isValid(authToken));
  }

  public login(credential: Credential): Observable<HttpResponseData> {
    return this.http.post('/auth/login', credential);
  }

  public checkExisting(username: string): Observable<any> {
    return this.http.get(`/users/check-existing/${username}`);
  }

  public signUp(data: CreateUserModel): Observable<any> {
    return this.http.post(`/users`, data);
  }

  public createUser(data: CreateUserModel): Observable<any> {
    return this.http.post(`/users`, data);
  }

  public getCurrentUser(): Observable<any> {
    return this.http.get('/account');
  }

  public logout() {
    this.clearSession();
  }

  public refreshToken(): Observable<HttpResponseData> {
    const authToken: AuthToken = this.getAuthToken();
    if (!authToken) {
      return;
    }
    const payload = { refreshtoken: authToken.refreshToken };
    return this.http.post(`/auth/refreshtoken`, { payload });
  }

  public scheduleRenewal() {
    if (!this.isAuthenticated()) {
      return;
    }
    this.unScheduleRenewal();
    const authToken: AuthToken = this.getAuthToken();
    const times = Math.max(1, authToken.expiresAt - Date.now() - (1000 * 30));

    this.refreshTokenSubscription = timer(times).pipe(
      switchMap(() => {
        return this.refreshToken();
      })
    ).subscribe((response) => {
      if (!response.AuthenticationResult) {
        return;
      }
      const token = new AuthToken(
        response.AuthenticationResult.AccessToken,
        response.AuthenticationResult.ExpiresIn
      );
      this.saveAuthToken(token);
    });
  }

  public saveSession(session: { authToken: AuthToken, user: User }): void {
    this.saveAuthToken(session.authToken);
    this.saveLoggedInUser(session.user);
  }

  public clearSession(): void {
    this.clearAuthToken();
    this.clearLoggedInUser();
    this.unScheduleRenewal();
  }

  public saveAuthToken(token: AuthToken): void {
    if (token instanceof AuthToken) {
      this.storage.setItem(env.authTokenKey, token);
      // this.scheduleRenewal();
    }
  }

  public clearAuthToken(): void {
    this.storage.removeItem(env.authTokenKey);
    this.unScheduleRenewal();
  }

  public getAuthToken(): AuthToken {
    return this.storage.getItem(env.authTokenKey);
  }

  public saveLoggedInUser(user: User): void {
    this.storage.setItem(env.loggedInUserKey, user);
  }

  public clearLoggedInUser(): void {
    this.storage.removeItem(env.loggedInUserKey);
  }

  public getloggedInUser(): User {
    return this.storage.getItem(env.loggedInUserKey);
  }

  private unScheduleRenewal() {
    if (this.refreshTokenSubscription) {
      this.refreshTokenSubscription.unsubscribe();
    }
  }
}
