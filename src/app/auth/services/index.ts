import { AuthService } from './auth.service';
import { AccountService } from './account.service';
import { SurveyService } from './survey.service';

export {
  AuthService,
  AccountService,
  SurveyService
};

export const SERVICES = [
  AuthService,
  AccountService,
  SurveyService
];
