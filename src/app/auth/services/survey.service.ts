import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment as env } from '@app/env';

@Injectable()
export class SurveyService {
  constructor(private http: HttpClient) {
  }

  saveSurvey(uid, survey) {
    return this.http.post(`${env.rootApi}/survey/${uid}`, survey);
  }

  getQuestions() {
    return this.http.get(`${env.rootApi}/questions`);
  }
}
