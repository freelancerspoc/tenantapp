import { Action } from '@ngrx/store';

import { Credential, AuthToken, User } from '../../models';

export enum AuthActionTypes {
  Login = '[Auth] Login',
  LoginSuccess = '[Auth] Login Success',
  LoginFailure = '[Auth] Login failrue',

  GetCurrentUser = '[Auth] Get current user profile',
  GetCurrentUserSuccess = '[Auth] Get current user profile success',
  GetCurrentUserFailure = '[Auth] Get current user profile failure',

  UpdateCurrentUserToLocal = '[Auth] Update current user profile to local',

  CheckExistingUser = '[Auth] Check existing user',
  CheckExistingUserSuccess = '[Auth] Check existing user success',
  CheckExistingUserFailure = '[Auth] Check existing user failure',

  CreateUser = '[Auth] Create User',
  CreateUserSuccess = '[Auth] Create User success',
  CreateUserFailure = '[Auth] Create User failure',

  Logout = '[Auth] Logout'
}

export class Login implements Action {
  readonly type = AuthActionTypes.Login;

  constructor(public payload: { credential: Credential }) { }
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LoginSuccess;

  constructor(public payload: { authToken: AuthToken }) { }
}

export class LoginFailure implements Action {
  readonly type = AuthActionTypes.LoginFailure;

  constructor(public payload: { error: any }) { }
}

export class GetCurrentUser implements Action {
  readonly type = AuthActionTypes.GetCurrentUser;
}

export class GetCurrentUserSuccess implements Action {
  readonly type = AuthActionTypes.GetCurrentUserSuccess;

  constructor(public payload: { userProfile: User }) { }
}

export class GetCurrentUserFailure implements Action {
  readonly type = AuthActionTypes.GetCurrentUserFailure;

  constructor(public payload: { error: any }) { }
}

export class UpdateCurrentUserToLocal implements Action {
  readonly type = AuthActionTypes.UpdateCurrentUserToLocal;

  constructor(public payload: { userProfile: any }) { }
}

export class CreateUser implements Action {
  readonly type = AuthActionTypes.CreateUser;

  constructor(public payload: { user: any }) { }
}

export class CreateUserSuccess implements Action {
  readonly type = AuthActionTypes.CreateUserSuccess;
}

export class CreateUserFailure implements Action {
  readonly type = AuthActionTypes.CreateUserFailure;

  constructor(public payload: { error: any }) { }
}

export class Logout implements Action {
  readonly type = AuthActionTypes.Logout;
}

export class CheckExistingUser implements Action {

  readonly type = AuthActionTypes.CheckExistingUser;

  constructor(public payload: { username: string }) { }
}

export class CheckExistingUserSuccess implements Action {
  readonly type = AuthActionTypes.CheckExistingUserSuccess;

  constructor(public payload: { userProfile: User }) { }
}

export class CheckExistingUserFailure implements Action {
  readonly type = AuthActionTypes.CheckExistingUserFailure;

  constructor(public payload: { error: any }) { }
}

export type AuthActions = Logout | UpdateCurrentUserToLocal |
  Login | LoginSuccess | LoginFailure |
  GetCurrentUser | GetCurrentUserSuccess | GetCurrentUserFailure |
  CheckExistingUser | CheckExistingUserSuccess | CheckExistingUserFailure |
  CreateUser | CreateUserSuccess | CreateUserFailure;

