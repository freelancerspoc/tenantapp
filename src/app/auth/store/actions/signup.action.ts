import { Action } from '@ngrx/store';

export enum SignupActionTypes {
  Signup = '[Auth] Signup',
  SignupSuccess = '[Auth] Signup success',
  SignupFailure = '[Auth] Signup failure'
}

export class Signup implements Action {
  readonly type = SignupActionTypes.Signup;

  constructor(public payload: { user: any }) { }
}

export class SignupSuccess implements Action {
  readonly type = SignupActionTypes.SignupSuccess;
}

export class SignupFailure implements Action {
  readonly type = SignupActionTypes.SignupFailure;

  constructor(public payload: { error: any }) { }
}

export type SignupActions =
  Signup | SignupSuccess | SignupFailure;
