import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { of } from 'rxjs';
import { tap, map, switchMap, catchError, withLatestFrom } from 'rxjs/operators';

import { selectRouterState } from '@app/core/store';
import { AuthService } from '../../services';
import { Credential, AuthToken } from '../../models';
import {
  AuthActionTypes,
  Login,
  LoginSuccess,
  LoginFailure,
  GetCurrentUser,
  GetCurrentUserSuccess,
  GetCurrentUserFailure,
  CheckExistingUser,
  CheckExistingUserSuccess,
  CheckExistingUserFailure,
  CreateUserFailure,
  CreateUserSuccess,
  CreateUser
} from '../actions/auth.actions';
import { AuthState } from '../reducers/auth.reducer';
import { NotificationService } from '@app/ui/notification';

@Injectable()
export class AuthEffects {

  @Effect()
  login$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.Login),
    map(action => action.payload.credential),
    switchMap((credential: Credential) =>
      this.auth.login(credential).pipe(
        map((res) => {
          return new LoginSuccess({ authToken: new AuthToken(res.accessToken, res.expiresIn) });
        }),
        catchError(error => {
          this.notification.error('Password is wrong!');
          return of(new LoginFailure({ error }));
        })
      )
    )
  );

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(
    ofType<LoginSuccess>(AuthActionTypes.LoginSuccess),
    map(action => action.payload.authToken),
    tap((authToken: AuthToken) => {
      this.auth.saveAuthToken(authToken);
    }),
    withLatestFrom(
      this.store.pipe(select(selectRouterState))
    ),
    tap(([, routerState]) => {
      const url = routerState.state.queryParams.returnUrl || '/dashboard';
      this.router.navigateByUrl(url);
    })
  );

  @Effect()
  onLoginSuccess$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginSuccess),
    map(() => {
      return new GetCurrentUser();
    })
  );

  @Effect()
  getCurrentUser$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.GetCurrentUser),
    switchMap(() =>
      this.auth.getCurrentUser().pipe(
        map((res) => {
          return new GetCurrentUserSuccess({ userProfile: res });
        }),
        catchError(error => {
          return of(new GetCurrentUserFailure({ error }));
        })
      )
    )
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType(AuthActionTypes.Logout),
    tap(() => {
      this.auth.clearSession();
    })
  );

  @Effect()
  checkExistingUser$ = this.actions$.pipe(
    ofType<CheckExistingUser>(AuthActionTypes.CheckExistingUser),
    map(action => action.payload.username),
    switchMap((username: string) => {
      return this.auth.checkExisting(username);
    }),
    map((res: any) => {
      return new CheckExistingUserSuccess({ userProfile: res });
    }),
    catchError(error => {
      return of(new CheckExistingUserFailure({ error }));
    })
  );

  @Effect()
  createUser$ = this.actions$.pipe(
    ofType<CreateUser>(AuthActionTypes.CreateUser),
    map(action => action.payload.user),
    switchMap((user) => {
      return this.auth.createUser(user).pipe(
        map(() => {
          return new CreateUserSuccess();
        }),
        catchError(error => {
          this.notification.error('Signup is fail!');
          return of(new CreateUserFailure({ error }));
        })
      );
    })

  );

  constructor(
    private actions$: Actions,
    private store: Store<AuthState>,
    private router: Router,
    private auth: AuthService,
    private notification: NotificationService
  ) { }
}
