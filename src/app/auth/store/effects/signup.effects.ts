import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { NotificationService } from '@app/ui/notification';
import { Signup, SignupActionTypes, SignupSuccess, SignupFailure } from '../actions/signup.action';
import { AuthService } from '../../services';

@Injectable()
export class SignupEffects {

  @Effect()
  signup$ = this.actions$.pipe(
    ofType<Signup>(SignupActionTypes.Signup),
    map(action => action.payload.user),
    switchMap((user) => {
      return this.auth.signUp(user).pipe(
        map(() => {
          this.router.navigate(['/signup/success']);
          return new SignupSuccess();
        }),
        catchError(error => {
          this.notification.error('Signup is fail!');
          return of(new SignupFailure({ error }));
        })
      );
    })

  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private auth: AuthService,
    private notification: NotificationService
  ) { }
}
