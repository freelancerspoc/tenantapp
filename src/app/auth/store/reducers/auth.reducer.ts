import { createFeatureSelector } from '@ngrx/store';

import { User } from '../../models';
import { AuthActions, AuthActionTypes } from '../actions/auth.actions';
import { SignupActions, SignupActionTypes } from '../actions/signup.action';

export interface AuthState {
  user: User | null;
  checkUser: any;
  loading: boolean;
  loaded: boolean;
  checking: boolean;
  signupPagePending: boolean;
  loginPagePending: boolean;
}

export const initialState: AuthState = {
  user: undefined,
  checkUser: undefined,
  loaded: false,
  loading: false,
  checking: false,
  signupPagePending: false,
  loginPagePending: false
};

export function authReducer(state = initialState, action: AuthActions | SignupActions): AuthState {
  switch (action.type) {

    case AuthActionTypes.Login: {
      return {
        ...state,
        loginPagePending: true
      };
    }

    case AuthActionTypes.LoginSuccess: {
      return {
        ...state,
        loginPagePending: false
      };
    }

    case AuthActionTypes.LoginFailure: {
      return {
        ...state,
        loginPagePending: false
      };
    }

    case AuthActionTypes.GetCurrentUserSuccess: {
      return {
        ...state,
        user: action.payload.userProfile
      };
    }

    case SignupActionTypes.Signup: {
      return {
        ...state,
        signupPagePending: true
      };
    }

    case SignupActionTypes.SignupSuccess: {
      return {
        ...state,
        signupPagePending: false
      };
    }

    case SignupActionTypes.SignupFailure: {
      return {
        ...state,
        signupPagePending: false
      };
    }

    case AuthActionTypes.CreateUser: {
      return {
        ...state
      };
    }

    case AuthActionTypes.CreateUserSuccess: {
      return {
        ...state
      };
    }

    case AuthActionTypes.CreateUserFailure: {
      return {
        ...state
      };
    }

    case AuthActionTypes.CheckExistingUser: {
      return {
        ...state,
        checkUser: undefined,
        checking: true
      };
    }

    case AuthActionTypes.CheckExistingUserSuccess: {
      return {
        ...state,
        checking: false,
        checkUser: action.payload.userProfile
      };
    }

    case AuthActionTypes.CheckExistingUserFailure: {
      return {
        ...state,
        checking: false,
        checkUser: null
      };
    }

    case AuthActionTypes.UpdateCurrentUserToLocal: {
      const newUser = {
        ...state.user,
        ...action.payload.userProfile
      };
      return {
        ...state,
        user: newUser
      };
    }

    case AuthActionTypes.Logout: {
      return {
        ...state,
        user: undefined
      };
    }

    default: {
      return state;
    }

  }
}
export const selectFeature = createFeatureSelector<AuthState>('auth');

export const getCurrentUser = (state: AuthState) => state.user;

export const getCheckUser = (state: AuthState) => state.checkUser;
export const getUserLoading = (state: AuthState) => state.loading;
export const getSignupPagePending = (state: AuthState) => state.signupPagePending;
export const getLoginPagePending = (state: AuthState) => state.loginPagePending;
export const getCheckingUserExist = (state: AuthState) => state.checking;
