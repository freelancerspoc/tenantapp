import { createSelector } from '@ngrx/store';

import * as fromReducer from '../reducers/auth.reducer';

export const selectCurrentUser = createSelector(
  fromReducer.selectFeature,
  ((state: fromReducer.AuthState) => {
    if (state.user) {
      return { ...state.user };
    }
    return null;
  })
);

export const selectCheckUser = createSelector(
    fromReducer.selectFeature,
    fromReducer.getCheckUser
);

export const selectUserLoading = createSelector(
  fromReducer.selectFeature,
  fromReducer.getUserLoading
);

export const selectSignupPagePending = createSelector(
    fromReducer.selectFeature,
    fromReducer.getSignupPagePending
);

export const selectLoginPagePending = createSelector(
    fromReducer.selectFeature,
    fromReducer.getLoginPagePending
);

export const selectCheckingUserExist = createSelector(
    fromReducer.selectFeature,
    fromReducer.getCheckingUserExist
);
