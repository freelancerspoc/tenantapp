import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpEventType
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { HttpResponseData } from '../response';

@Injectable()
export class HttpResponseInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      filter((event: HttpEvent<any>) => {
        return event.type === HttpEventType.Response;
      }),
      map((response: HttpResponse<any>) => {

        if (response.body.data) {
          const dataResponse: HttpResponseData = {
            ...response.body,
            data: response.body.data
          };
          return response.clone({ body: dataResponse });
        }

        if (response.body.error) {
          try {
            const error = JSON.parse(response.body.error);
            const dataResponse: HttpResponseData = { error };
            return response.clone({ body: dataResponse });
          } catch (error) {
            return response;
          }
        }
        return response;
      })
    );
  }
}
