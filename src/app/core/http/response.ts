export interface HttpResponseData {
  data?: any;
  error?: any;
  [x: string]: any;
}
