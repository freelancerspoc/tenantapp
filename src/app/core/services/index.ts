import { PageTitleService } from './page-title/page-title.service';
import { LocalStorageService } from './web-storage/local-storage.service';
import { SplashScreenService } from './splash-screen.service';

export {
  PageTitleService,
  LocalStorageService,
  SplashScreenService
};

export const SERVICES = [
  PageTitleService,
  LocalStorageService,
  SplashScreenService
];
