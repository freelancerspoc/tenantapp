import { Action } from '@ngrx/store';

export enum LayoutActionTypes {
  TOGGLE_LEFT_SIDEBAR = '[LAYOUT] Toggle left sidebar'
}

export class ToggleLeftSidebar implements Action {
  readonly type = LayoutActionTypes.TOGGLE_LEFT_SIDEBAR;
}

export type LayoutActionsUnion = ToggleLeftSidebar;

