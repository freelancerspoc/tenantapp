import { LayoutActions } from '../actions';

export interface State {
  leftSidebar: {
    collapsed: boolean
  };
}

export const initialState: State = {
  leftSidebar: {
    collapsed: false
  }
};

export function reducer(state = initialState, action: | LayoutActions.LayoutActionsUnion): State {
  switch (action.type) {
    case LayoutActions.LayoutActionTypes.TOGGLE_LEFT_SIDEBAR: {
      const collapsed = state.leftSidebar.collapsed;
      return {
        ...state,
        leftSidebar: {
          collapsed: !collapsed
        }
      };
    }

    default: {
      return state;
    }
  }
}
