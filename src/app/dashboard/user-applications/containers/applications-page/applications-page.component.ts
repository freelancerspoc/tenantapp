import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';
import { AppState } from '@app/core/store';

import * as _ from 'lodash';

import { User } from '@app/auth';

import { selectCurrentUser } from 'src/app/auth/store/selectors/auth.selector';
import { UpdateApplications, UploadPhoto, UploadCoverPhoto } from '../../store/actions/user-applications.actions';
import { selectUserApplicationsLoading } from '../../store/selectors/user-applications.selector';

@Component({
  selector: 'app-applications-page',
  templateUrl: './applications-page.component.html',
  styleUrls: ['./applications-page.component.scss']
})
export class ApplicationsPageComponent {

  loading$: Observable<boolean>;
  user$: Observable<User>;
  constructor(
    private router: Router,
    private store: Store<AppState>
  ) {

    this.loading$ = this.store.pipe(select(selectUserApplicationsLoading));
    this.user$ = this.store.pipe(select(selectCurrentUser));
  }

  onUpload($event) {
    if ($event.type === 1) {
      this.store.dispatch(new UploadPhoto({ formData: $event.formData }));
    }
    if ($event.type === 2) {
      this.store.dispatch(new UploadCoverPhoto({ formData: $event.formData }));
    }


  }

  onCancel() {
    this.router.navigate(['/dashboard']);
  }

  onSave(user: User): void {
    this.store.dispatch(new UpdateApplications({ user: user }));
  }

}
