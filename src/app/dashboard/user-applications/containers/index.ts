import { ApplicationsPageComponent } from './applications-page/applications-page.component';

export {
    ApplicationsPageComponent
};

export const CONTAINERS = [
    ApplicationsPageComponent
];
