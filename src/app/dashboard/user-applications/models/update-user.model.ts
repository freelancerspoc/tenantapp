export interface UpdateUserModel {
    id: string;
    avatar: string;
    coverPhoto: string;
    firstName: string;
    lastName: string;
    location: string;
    bio: string;
    angelList: string;
    facebook: string;
    twitter: string;
    linkedIn: string;
    website: string;
    hideMyApplications: boolean;
}
