import { UserApplicationsService } from './user-applications.service';

export {
    UserApplicationsService
};

export const SERVICES = [
    UserApplicationsService
];
