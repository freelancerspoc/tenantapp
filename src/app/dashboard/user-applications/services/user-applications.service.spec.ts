import { TestBed } from '@angular/core/testing';

import { UserApplicationsService } from './user-applications.service';

describe('UserApplicationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserApplicationsService = TestBed.get(UserApplicationsService);
    expect(service).toBeTruthy();
  });
});
