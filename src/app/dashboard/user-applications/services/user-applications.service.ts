import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment as env } from '@app/env';
import { UpdateUserModel } from '../models';
import { LocalStorageService } from '@app/core/services';

@Injectable()
export class UserApplicationsService {

  constructor(private http: HttpClient, private storage: LocalStorageService) { }

  updateApplications(data: UpdateUserModel): Observable<any> {
    return this.http.patch(`/account`, data);
  }

  getCurrentUser(): Observable<any> {
    return this.http.get(`/account`);
  }

  uploadPhoto(mediaLink: any): Observable<any> {
    return this.http.post(`/files/upload`, mediaLink);
  }

}
