import { Action } from '@ngrx/store';

export enum UserApplicationsActionTypes {

  UpdateApplications = '[User Applications] Update applications',
  UpdateApplicationsSuccess = '[User Applications] Update applications success',
  UpdateApplicationsFailure = '[User Applications] Update applications failure',

  UploadPhoto = '[User Applications] Upload photo',
  UploadPhotoSuccess = '[User Applications] Upload photo success',
  UploadPhotoFailure = '[User Applications] Upload photo failure',

  UploadCoverPhoto = '[User Applications] Upload cover photo',
  UploadCoverPhotoSuccess = '[User Applications] Upload cover photo success',
  UploadCoverPhotoFailure = '[User Applications] Upload cover photo failure',
}

export class UpdateApplications implements Action {
  readonly type = UserApplicationsActionTypes.UpdateApplications;

  constructor(public payload: { user: any }) { }
}

export class UpdateApplicationsSuccess implements Action {
  readonly type = UserApplicationsActionTypes.UpdateApplicationsSuccess;

  constructor(public payload: { user: any }) { }
}

export class UpdateApplicationsFailure implements Action {
  readonly type = UserApplicationsActionTypes.UpdateApplicationsFailure;

  constructor(public payload: { error: any }) { }
}

export class UploadPhoto implements Action {
  readonly type = UserApplicationsActionTypes.UploadPhoto;

  constructor(public payload: { formData: FormData }) { }
}

export class UploadPhotoSuccess implements Action {
  readonly type = UserApplicationsActionTypes.UploadPhotoSuccess;

  constructor(public payload: { mediaLink: string }) { }
}

export class UploadPhotoFailure implements Action {
  readonly type = UserApplicationsActionTypes.UploadPhotoFailure;

  constructor(public payload: { error: any }) { }
}

export class UploadCoverPhoto implements Action {
  readonly type = UserApplicationsActionTypes.UploadCoverPhoto;

  constructor(public payload: { formData: FormData }) { }
}

export class UploadCoverPhotoSuccess implements Action {
  readonly type = UserApplicationsActionTypes.UploadCoverPhotoSuccess;

  constructor(public payload: { mediaLink: string }) { }
}

export class UploadCoverPhotoFailure implements Action {
  readonly type = UserApplicationsActionTypes.UploadCoverPhotoFailure;

  constructor(public payload: { error: any }) { }
}


export type UserApplicationsActions =
  UpdateApplications | UpdateApplicationsSuccess | UpdateApplicationsFailure |
  UploadPhoto | UploadPhotoSuccess | UploadPhotoFailure |
  UploadCoverPhoto | UploadCoverPhotoSuccess | UploadCoverPhotoFailure;
