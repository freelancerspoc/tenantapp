import { UserApplicationsActions, UserApplicationsActionTypes } from '../actions/user-applications.actions';
import { createFeatureSelector } from '@ngrx/store';

export interface UserApplicationsState {
  user: any | null;
  loaded: boolean;
  loading: boolean;
  loadingPhoto: boolean;
  loadedPhoto: boolean;
}

export const initialState: UserApplicationsState = {
  user: null,
  loaded: false,
  loading: false,
  loadingPhoto: false,
  loadedPhoto: false
};

export function userApplicationsReducer(state = initialState, action: UserApplicationsActions): UserApplicationsState {
  switch (action.type) {

    case UserApplicationsActionTypes.UpdateApplications: {
      return {
        ...state,
        loaded: false,
        loading: true
      };
    }

    case UserApplicationsActionTypes.UpdateApplicationsSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false,
        user: action.payload.user
      };
    }

    case UserApplicationsActionTypes.UpdateApplicationsFailure: {
      return {
        ...state,
        loaded: false,
        loading: false
      };
    }
    case UserApplicationsActionTypes.UploadPhoto: {
      return {
        ...state,
        loadedPhoto: false,
        loadingPhoto: true
      };
    }

    case UserApplicationsActionTypes.UploadPhotoSuccess: {
      return {
        ...state,
        loadedPhoto: true,
        loadingPhoto: false
      };
    }

    case UserApplicationsActionTypes.UploadPhotoFailure: {
      return {
        ...state,
        loadedPhoto: false,
        loadingPhoto: false
      };
    }

    case UserApplicationsActionTypes.UploadCoverPhoto: {
      return {
        ...state,
        loadedPhoto: false,
        loadingPhoto: true
      };
    }

    case UserApplicationsActionTypes.UploadCoverPhotoSuccess: {
      return {
        ...state,
        loadedPhoto: true,
        loadingPhoto: false
      };
    }

    case UserApplicationsActionTypes.UploadCoverPhotoFailure: {
      return {
        ...state,
        loadedPhoto: false,
        loadingPhoto: false
      };
    }
    default: {
      return state;
    }

  }
}

export const selectFeature = createFeatureSelector<UserApplicationsState>('user-applications');
export const getUserApplicationsLoading = (state: UserApplicationsState) => state.loading;
export const getUserApplicationsLoaded = (state: UserApplicationsState) => state.loaded;

