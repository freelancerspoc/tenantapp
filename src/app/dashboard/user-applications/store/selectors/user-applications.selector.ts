import { createSelector } from '@ngrx/store';

import * as fromReducer from '../reducers/user-applications.reducer';

export const selectUserApplicationsLoading = createSelector(
    fromReducer.selectFeature,
    fromReducer.getUserApplicationsLoading
);

export const selectUserApplicationsLoaded = createSelector(
    fromReducer.selectFeature,
    fromReducer.getUserApplicationsLoaded
);
