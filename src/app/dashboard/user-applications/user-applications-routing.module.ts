import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationsPageComponent } from './containers';

const routes: Routes = [
  {
    path: '',
    component: ApplicationsPageComponent,
    data: {
      title: 'Applications'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserApplicationsRoutingModule { }
