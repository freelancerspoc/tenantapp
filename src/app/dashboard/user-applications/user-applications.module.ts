import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { NotificationModule } from '@app/ui/notification';
import { UserApplicationsRoutingModule } from './user-applications-routing.module';
import * as fromUserApplications from './store/reducers/user-applications.reducer';
import { UserApplicationsEffects } from './store/effects/user-applications.effects';
import { CONTAINERS } from './containers';
import { SERVICES } from './services';
import { COMPONENTS } from './components';

@NgModule({
  declarations: [
    ...CONTAINERS,
    ...COMPONENTS
  ],
  imports: [
    CommonModule,
    FormsModule,
    FileUploadModule,
    NotificationModule,
    UserApplicationsRoutingModule,
    StoreModule.forFeature('user-applications', fromUserApplications.userApplicationsReducer),
    EffectsModule.forFeature([UserApplicationsEffects])
  ],
  providers: [
    ...SERVICES
  ]
})
export class UserApplicationsModule { }
