import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

import { User } from '@app/auth';
import { Store } from '@ngrx/store';
import { AppState } from '@app/core/store';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileFormComponent implements OnInit {

  @Input() user: User;
  @Input() loading: boolean;
  @Output() upload: EventEmitter<any>;
  @Output() save: EventEmitter<any>;
  @Output() cancel: EventEmitter<any>;
  loading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
    this.upload = new EventEmitter();
    this.save = new EventEmitter();
    this.cancel = new EventEmitter();
  }

  ngOnInit() {
    console.log(this.user);
  }

  onFileChange($event: any, type) {

    if ($event.target.files && $event.target.files[0]) {
      const formData = new FormData();
      const file = $event.target.files[0];
      formData.append('file', file);
      const data = {
        formData: formData,
        type: type,
      };
      this.upload.emit(data);
    }
  }

  onCancel() {
    this.cancel.emit();
  }

  onSave() {
    this.save.emit({ ...this.user });
  }

}
