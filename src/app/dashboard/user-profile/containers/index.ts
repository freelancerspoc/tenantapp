import { ProfilePageComponent } from './profile-page/profile-page.component';

export {
    ProfilePageComponent
};

export const CONTAINERS = [
    ProfilePageComponent
];
