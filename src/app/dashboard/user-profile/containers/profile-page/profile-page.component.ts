import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';
import { AppState } from '@app/core/store';

import * as _ from 'lodash';

import { User } from '@app/auth';

import { selectCurrentUser } from 'src/app/auth/store/selectors/auth.selector';
import { UpdateProfile, UploadPhoto, UploadCoverPhoto } from '../../store/actions/user-profile.actions';
import { selectUserProfileLoading } from '../../store/selectors/user-profile.selector';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent {

  loading$: Observable<boolean>;
  user$: Observable<User>;
  constructor(
    private router: Router,
    private store: Store<AppState>
  ) {

    this.loading$ = this.store.pipe(select(selectUserProfileLoading));
    this.user$ = this.store.pipe(select(selectCurrentUser));
  }

  onUpload($event) {
    if ($event.type === 1) {
      this.store.dispatch(new UploadPhoto({ formData: $event.formData }));
    }
    if ($event.type === 2) {
      this.store.dispatch(new UploadCoverPhoto({ formData: $event.formData }));
    }


  }

  onCancel() {
    this.router.navigate(['/dashboard']);
  }

  onSave(user: User): void {
    this.store.dispatch(new UpdateProfile({ user: user }));
  }

}
