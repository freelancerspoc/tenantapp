import { UserProfileService } from './user-profile.service';

export {
    UserProfileService
};

export const SERVICES = [
    UserProfileService
];
