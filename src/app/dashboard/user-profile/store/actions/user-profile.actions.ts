import { Action } from '@ngrx/store';

export enum UserProfileActionTypes {

  UpdateProfile = '[User Profile] Update profile',
  UpdateProfileSuccess = '[User Profile] Update profile success',
  UpdateProfileFailure = '[User Profile] Update profile failure',

  UploadPhoto = '[User Profile] Upload photo',
  UploadPhotoSuccess = '[User Profile] Upload photo success',
  UploadPhotoFailure = '[User Profile] Upload photo failure',

  UploadCoverPhoto = '[User Profile] Upload cover photo',
  UploadCoverPhotoSuccess = '[User Profile] Upload cover photo success',
  UploadCoverPhotoFailure = '[User Profile] Upload cover photo failure',
}

export class UpdateProfile implements Action {
  readonly type = UserProfileActionTypes.UpdateProfile;

  constructor(public payload: { user: any }) { }
}

export class UpdateProfileSuccess implements Action {
  readonly type = UserProfileActionTypes.UpdateProfileSuccess;

  constructor(public payload: { user: any }) { }
}

export class UpdateProfileFailure implements Action {
  readonly type = UserProfileActionTypes.UpdateProfileFailure;

  constructor(public payload: { error: any }) { }
}

export class UploadPhoto implements Action {
  readonly type = UserProfileActionTypes.UploadPhoto;

  constructor(public payload: { formData: FormData }) { }
}

export class UploadPhotoSuccess implements Action {
  readonly type = UserProfileActionTypes.UploadPhotoSuccess;

  constructor(public payload: { mediaLink: string }) { }
}

export class UploadPhotoFailure implements Action {
  readonly type = UserProfileActionTypes.UploadPhotoFailure;

  constructor(public payload: { error: any }) { }
}

export class UploadCoverPhoto implements Action {
  readonly type = UserProfileActionTypes.UploadCoverPhoto;

  constructor(public payload: { formData: FormData }) { }
}

export class UploadCoverPhotoSuccess implements Action {
  readonly type = UserProfileActionTypes.UploadCoverPhotoSuccess;

  constructor(public payload: { mediaLink: string }) { }
}

export class UploadCoverPhotoFailure implements Action {
  readonly type = UserProfileActionTypes.UploadCoverPhotoFailure;

  constructor(public payload: { error: any }) { }
}


export type UserProfileActions =
  UpdateProfile | UpdateProfileSuccess | UpdateProfileFailure |
  UploadPhoto | UploadPhotoSuccess | UploadPhotoFailure |
  UploadCoverPhoto | UploadCoverPhotoSuccess | UploadCoverPhotoFailure;
