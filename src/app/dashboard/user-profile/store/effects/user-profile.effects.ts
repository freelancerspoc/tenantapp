import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import * as _ from 'lodash';
import { NotificationService } from '@app/ui/notification';
import { UserProfileService } from '../../services';
import {
  UpdateProfile,
  UserProfileActionTypes,
  UpdateProfileSuccess,
  UpdateProfileFailure,
  UploadPhoto,
  UploadPhotoSuccess,
  UploadPhotoFailure,
  UploadCoverPhotoSuccess,
  UploadCoverPhotoFailure,
  UploadCoverPhoto
} from '../actions/user-profile.actions';
import { UpdateCurrentUserToLocal } from '@app/auth';
import { environment as env } from '@app/env';

@Injectable()
export class UserProfileEffects {

  @Effect()
  updateUser$ = this.actions$.pipe(
    ofType<UpdateProfile>(UserProfileActionTypes.UpdateProfile),
    map(action => action.payload.user),
    switchMap((user) =>
      this.userProfileService.updateProfile(user).pipe(
        map(() => {
          this.notification.success('Update user successfully.');
          return new UpdateProfileSuccess({ user: user });
        }),
        catchError(error => {
          this.notification.error('Update user fail.');
          return of(new UpdateProfileFailure({ error }));
        })
      )
    )
  );

  @Effect()
  updateUserSuccess$ = this.actions$.pipe(
    ofType<UpdateProfile>(UserProfileActionTypes.UpdateProfileSuccess),
    map((action: any) => {
      return new UpdateCurrentUserToLocal({ userProfile: action.payload.user });
    }),

  );

  @Effect()
  uploadPhoto$ = this.actions$.pipe(
    ofType<UploadPhoto>(UserProfileActionTypes.UploadPhoto),
    map(action => action.payload),
    switchMap((data) =>
      this.userProfileService.uploadPhoto(data.formData).pipe(
        map((res) => {
          this.notification.success('Upload avatar successfully.');
          const imageUrl = env.imageUrl + res.reference;
          return new UploadPhotoSuccess({ mediaLink: imageUrl });
        }),
        catchError(error => {
          this.notification.error('Upload avatar fail.');
          return of(new UploadPhotoFailure({ error }));
        })
      )
    )
  );

  @Effect()
  uploadPhotoSuccess$ = this.actions$.pipe(
    ofType<UploadPhotoSuccess>(UserProfileActionTypes.UploadPhotoSuccess),
    map((action: any) => {
      return new UpdateProfile({
        user: {
          avatar: action.payload.mediaLink
        }
      });
    }),
  );

  @Effect()
  uploadCoverPhoto$ = this.actions$.pipe(
    ofType<UploadCoverPhoto>(UserProfileActionTypes.UploadCoverPhoto),
    map(action => action.payload),
    switchMap((data) =>
      this.userProfileService.uploadPhoto(data.formData).pipe(
        map((res) => {
          this.notification.success('Upload cover photo successfully.');
          const imageUrl = env.imageUrl + res.reference;
          return new UploadCoverPhotoSuccess({ mediaLink: imageUrl });
        }),
        catchError(error => {
          this.notification.error('Upload cover photo fail.');
          return of(new UploadCoverPhotoFailure({ error }));
        })
      )
    )
  );

  @Effect()
  uploadCoverPhotoSuccess$ = this.actions$.pipe(
    ofType<UploadCoverPhotoSuccess>(UserProfileActionTypes.UploadCoverPhotoSuccess),
    map((action: any) => {
      return new UpdateProfile({
        user: {
          coverPhoto: action.payload.mediaLink
        }
      });
    }),
  );
  constructor(
    private actions$: Actions,
    private notification: NotificationService,
    private userProfileService: UserProfileService
  ) { }
}
