import { UserProfileActions, UserProfileActionTypes } from '../actions/user-profile.actions';
import { createFeatureSelector } from '@ngrx/store';

export interface UserProfileState {
  user: any | null;
  loaded: boolean;
  loading: boolean;
  loadingPhoto: boolean;
  loadedPhoto: boolean;
}

export const initialState: UserProfileState = {
  user: null,
  loaded: false,
  loading: false,
  loadingPhoto: false,
  loadedPhoto: false
};

export function userProfileReducer(state = initialState, action: UserProfileActions): UserProfileState {
  switch (action.type) {

    case UserProfileActionTypes.UpdateProfile: {
      return {
        ...state,
        loaded: false,
        loading: true
      };
    }

    case UserProfileActionTypes.UpdateProfileSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false,
        user: action.payload.user
      };
    }

    case UserProfileActionTypes.UpdateProfileFailure: {
      return {
        ...state,
        loaded: false,
        loading: false
      };
    }
    case UserProfileActionTypes.UploadPhoto: {
      return {
        ...state,
        loadedPhoto: false,
        loadingPhoto: true
      };
    }

    case UserProfileActionTypes.UploadPhotoSuccess: {
      return {
        ...state,
        loadedPhoto: true,
        loadingPhoto: false
      };
    }

    case UserProfileActionTypes.UploadPhotoFailure: {
      return {
        ...state,
        loadedPhoto: false,
        loadingPhoto: false
      };
    }

    case UserProfileActionTypes.UploadCoverPhoto: {
      return {
        ...state,
        loadedPhoto: false,
        loadingPhoto: true
      };
    }

    case UserProfileActionTypes.UploadCoverPhotoSuccess: {
      return {
        ...state,
        loadedPhoto: true,
        loadingPhoto: false
      };
    }

    case UserProfileActionTypes.UploadCoverPhotoFailure: {
      return {
        ...state,
        loadedPhoto: false,
        loadingPhoto: false
      };
    }
    default: {
      return state;
    }

  }
}

export const selectFeature = createFeatureSelector<UserProfileState>('user-profile');
export const getUserProfileLoading = (state: UserProfileState) => state.loading;
export const getUserProfileLoaded = (state: UserProfileState) => state.loaded;

