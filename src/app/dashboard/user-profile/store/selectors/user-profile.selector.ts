import { createSelector } from '@ngrx/store';

import * as fromReducer from '../reducers/user-profile.reducer';

export const selectUserProfileLoading = createSelector(
    fromReducer.selectFeature,
    fromReducer.getUserProfileLoading
);

export const selectUserProfileLoaded = createSelector(
    fromReducer.selectFeature,
    fromReducer.getUserProfileLoaded
);
