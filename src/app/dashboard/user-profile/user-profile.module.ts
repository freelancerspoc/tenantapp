import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { NotificationModule } from '@app/ui/notification';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import * as fromUserProfile from './store/reducers/user-profile.reducer';
import { UserProfileEffects } from './store/effects/user-profile.effects';
import { CONTAINERS } from './containers';
import { SERVICES } from './services';
import { COMPONENTS } from './components';

@NgModule({
  declarations: [
    ...CONTAINERS,
    ...COMPONENTS
  ],
  imports: [
    CommonModule,
    FormsModule,
    FileUploadModule,
    NotificationModule,
    UserProfileRoutingModule,
    StoreModule.forFeature('user-profile', fromUserProfile.userProfileReducer),
    EffectsModule.forFeature([UserProfileEffects])
  ],
  providers: [
    ...SERVICES
  ]
})
export class UserProfileModule { }
