import { ProfileSettingsFormComponent } from './profile-settings-form/profile-settings-form.component';
import { UserAvatarComponent } from './user-avatar/user-avatar.component';

export const COMPONENTS = [
    ProfileSettingsFormComponent,
    UserAvatarComponent
];
