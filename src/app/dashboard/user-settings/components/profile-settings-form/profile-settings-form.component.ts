import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { User } from '@app/auth';
import { AppState } from '@app/core/store';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-profile-settings-form',
  templateUrl: './profile-settings-form.component.html',
  styleUrls: ['./profile-settings-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileSettingsFormComponent {

  @Input() user: User;
  @Input() loading: boolean;
  @Output() save: EventEmitter<any>;

  constructor(private store: Store<AppState>) {
    this.save = new EventEmitter();
  }

  onSubmit() {
    this.save.emit({
      firstName: this.user.firstName,
      lastName: this.user.lastName
    });
  }

}
