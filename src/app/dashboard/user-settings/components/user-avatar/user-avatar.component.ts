import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';

import * as _ from 'lodash';

import { User } from '@app/auth';
import { NotificationService } from '@app/ui/notification';

@Component({
  selector: 'app-user-avatar',
  templateUrl: './user-avatar.component.html',
  styleUrls: ['./user-avatar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserAvatarComponent implements OnInit {

  @Input() user: User;
  @Input() avatar: any;

  @Output() upload: EventEmitter<FormData>;

  private fileName = '';

  imageChangedEvent: any = '';
  cropperReady = false;
  showPopup = false;
  imageFile: any;

  constructor(private notification: NotificationService) {
    this.upload = new EventEmitter();
  }

  ngOnInit() {
    this.avatar = this.avatar ? this.avatar : '/assets/img/avatar-default.png';
  }

  onFileChange($event) {
    this.showPopup = true;
    this.imageChangedEvent = $event;
    this.imageFile = $event;
    if ($event.target.files.length > 0) {
      this.fileName = $event.target.files[0].name;
    }
  }

  imageCropped($event) {
    this.imageFile = $event;
  }

  imageLoaded() {
    this.cropperReady = true;
  }

  loadImageFailed() {
    this.cropperReady = false;
    this.notification.error('Image is defective. Please select another image.');
  }

  onImageCroppedFile($event) {
    this.imageFile = $event;
  }

  onClose() {
    this.showPopup = false;
  }

  onSave() {
    this.showPopup = false;
    const formData = new FormData();
    formData.append('file', this.imageFile, this.fileName);
    const reader = new FileReader();
    reader.onload = (event: any) => {
      this.avatar = event.target.result;
      this.upload.emit(formData);
    };
    reader.readAsDataURL(this.imageFile);
  }
}




