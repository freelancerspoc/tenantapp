import { Component, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from '@app/core/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import * as _ from 'lodash';
import { selectCurrentUser } from '@app/auth';

import { ChangeEmail } from '../../store/actions/user-settings.actions';
import { selectUserSettingsLoading } from '../../store/selectors/user-settings.selector';

@Component({
  selector: 'app-change-email-page',
  templateUrl: './change-email-page.component.html',
  styleUrls: ['./change-email-page.component.scss']
})
export class ChangeEmailPageComponent implements OnDestroy {

  userModel: any = {};
  loading$: Observable<boolean>;
  private componentDestroyed$: Subject<boolean>;

  constructor(private store: Store<AppState>) {
    this.componentDestroyed$ = new Subject();
    this.loading$ = this.store.pipe(select(selectUserSettingsLoading));
    this.store.pipe(
      select(selectCurrentUser),
      takeUntil(this.componentDestroyed$)
    ).subscribe((user: any) => {
      if (user) {
        this.userModel = _.cloneDeep(user);
      }
    });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  onSubmit() {
    this.store.dispatch(new ChangeEmail({
      email: this.userModel.email,
      password: this.userModel.password
    }));
  }

}
