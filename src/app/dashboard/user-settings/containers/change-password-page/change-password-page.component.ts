import { Component, OnDestroy, ViewChild } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from '@app/core/store';
import { Observable, Subject } from 'rxjs';

import * as _ from 'lodash';
import { ChangePassword } from '../../store/actions/user-settings.actions';
import { selectUserSettingsLoading, selectIsChangePassword } from '../../store/selectors/user-settings.selector';
import { GetCurrentUser } from 'src/app/auth/store/actions/auth.actions';
import { selectCurrentUser } from 'src/app/auth/store/selectors/auth.selector';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-change-password-page',
  templateUrl: './change-password-page.component.html',
  styleUrls: ['./change-password-page.component.scss']
})
export class ChangePasswordPageComponent implements OnDestroy {
  @ViewChild('form') formValues;

  userModel: any = {};
  showMessage = false;
  errorMesssage = '';
  checkPassword = true;
  loading$: Observable<boolean>;
  private componentDestroyed$: Subject<boolean>;

  constructor(private store: Store<AppState>) {
    this.componentDestroyed$ = new Subject();
    this.store.dispatch(new GetCurrentUser);
    this.loading$ = this.store.pipe(select(selectUserSettingsLoading));
    this.store.pipe(
      select(selectCurrentUser),
      takeUntil(this.componentDestroyed$)
    ).subscribe((user: any) => {
      if (user) {
        this.userModel = _.cloneDeep(user);
      }
    });
    this.store.pipe(select(selectIsChangePassword),
      takeUntil(this.componentDestroyed$)).subscribe(res => {
        if (res) {
          this.formValues.resetForm();
        }
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  onSubmit() {
    this.store.dispatch(new ChangePassword({
      password: this.userModel.password,
      newPassword: this.userModel.newPassword
    }));
  }

  comparePassword() {
    if (this.userModel.confirmPassword === '') {
      this.errorMesssage = '';
      this.checkPassword = false;
    } else if (this.userModel.newPassword !== this.userModel.confirmPassword) {
      this.errorMesssage = 'Password Confirm is mismatched';
    } else {
      this.errorMesssage = '';
      this.checkPassword = false;
    }
  }

  inputNewPassword() {
    this.errorMesssage = '';
  }

}
