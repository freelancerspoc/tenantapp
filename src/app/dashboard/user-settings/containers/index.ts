import { SettingsPageComponent } from './settings-page/settings-page.component';
import { ChangePasswordPageComponent } from './change-password-page/change-password-page.component';
import { PaymentPageComponent } from './payment-page/payment-page.component';
import { ChangeEmailPageComponent } from './change-email-page/change-email-page.component';
import { SettingProfilePageComponent } from './setting-profile-page/setting-profile-page.component';
import { UpdatePaymentPageComponent } from './update-payment-page/update-payment-page.component';
import { ProceedPaymentPageComponent } from './proceed-payment-page/proceed-payment-page.component';

export {
    SettingsPageComponent,
    ChangePasswordPageComponent,
    PaymentPageComponent,
    ChangeEmailPageComponent,
    SettingProfilePageComponent,
    UpdatePaymentPageComponent,
    ProceedPaymentPageComponent
};

export const CONTAINERS = [
    SettingsPageComponent,
    ChangePasswordPageComponent,
    PaymentPageComponent,
    ChangeEmailPageComponent,
    SettingProfilePageComponent,
    UpdatePaymentPageComponent,
    ProceedPaymentPageComponent
];
