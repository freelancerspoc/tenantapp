import { Component } from '@angular/core';

@Component({
  selector: 'app-payment-page',
  templateUrl: './payment-page.component.html',
  styleUrls: ['./payment-page.component.scss']
})
export class PaymentPageComponent {

  showPaypal = false;
  showOption = true;
  showProceedPaymentPage = false;

  constructor() {
  }

  addNewAccount() {
    this.showPaypal = true;
    this.showOption = false;
  }

  onPay() {
    this.showProceedPaymentPage = true;
    this.showPaypal = false;
    this.showOption = false;
  }

}
