import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProceedPaymentPageComponent } from './proceed-payment-page.component';

describe('ProceedPaymentPageComponent', () => {
  let component: ProceedPaymentPageComponent;
  let fixture: ComponentFixture<ProceedPaymentPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProceedPaymentPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProceedPaymentPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
