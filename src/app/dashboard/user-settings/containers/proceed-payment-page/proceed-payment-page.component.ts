import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';

import { environment as env } from '@app/env';


@Component({
  selector: 'app-proceed-payment-page',
  templateUrl: './proceed-payment-page.component.html',
  styleUrls: ['./proceed-payment-page.component.scss']
})
export class ProceedPaymentPageComponent implements OnInit {

  public payPalConfig?: PayPalConfig;
  showPayment = true;
  showPaypal = false;
  total = 0.01;
  constructor() { }

  ngOnInit() {
    this.initConfig();
  }

  onContinue() {
    this.showPayment = false;
    this.showPaypal = true;
  }

  private initConfig(): void {
    this.payPalConfig = new PayPalConfig(
      PayPalIntegrationType.ClientSideREST,
      PayPalEnvironment.Sandbox,
      {
        commit: true,
        client: {
          sandbox: env.sanboxClientId
        },
        button: {
          label: 'paypal',
          layout: 'vertical'
        },
        onAuthorize: (data, actions) => {
          return actions.payment.execute().then(() => {
            window.alert('Payment Complete!');
          });
        },
        onPaymentComplete: (data, actions) => {
          return actions.payment.create({
            transactions: [{
              amount: {
                total: this.total,
                currency: 'USD'
              }
            }]
          });
        },
        payment: () => {
          return of(undefined);
        },
        experience: {
          noShipping: true,
          brandName: 'Client App'
        },
        transactions: [{
          amount: {
            currency: 'USD',
            total: this.total
          }
        }],
        note_to_payer: 'Contact us if you have troubles processing payment'
      }
    );
  }

}
