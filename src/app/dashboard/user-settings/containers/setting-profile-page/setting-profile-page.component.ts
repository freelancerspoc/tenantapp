import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as _ from 'lodash';
import { Observable } from 'rxjs';

import { AppState } from '@app/core/store';
import { User, selectCurrentUser } from '@app/auth';

import { UpdateProfileSettings, UploadPhoto } from '../../store/actions/user-settings.actions';
import { selectUserSettingsLoading } from '../../store/selectors/user-settings.selector';

@Component({
  selector: 'app-setting-profile-page',
  templateUrl: './setting-profile-page.component.html',
  styleUrls: ['./setting-profile-page.component.scss']
})
export class SettingProfilePageComponent {
  user$: Observable<User>;
  loading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
    this.user$ = this.store.pipe(select(selectCurrentUser));
    this.loading$ = this.store.pipe(select(selectUserSettingsLoading));
  }

  onUpload($event: FormData) {
    this.store.dispatch(new UploadPhoto({ formData: $event }));
  }

  onSave(user: User): void {
    this.store.dispatch(new UpdateProfileSettings({ user: user }));
  }
}
