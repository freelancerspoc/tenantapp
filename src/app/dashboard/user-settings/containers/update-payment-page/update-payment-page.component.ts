import { Component, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from '@app/core/store';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import * as _ from 'lodash';

import { selectCurrentUser } from '@app/auth';

import { Country } from '../../models';
import { selectUserSettingsLoading } from '../../store/selectors/user-settings.selector';
import { UpdatePayment } from '../../store/actions/user-settings.actions';

@Component({
  selector: 'app-update-payment-page',
  templateUrl: './update-payment-page.component.html',
  styleUrls: ['./update-payment-page.component.scss']
})
export class UpdatePaymentPageComponent implements OnDestroy {

  showEditPaypal = false;
  preferredCountries: Country[];
  userModel: any = {};
  loading$: Observable<boolean>;
  private componentDestroyed$: Subject<boolean>;

  constructor(private store: Store<AppState>, private router: Router) {
    this.componentDestroyed$ = new Subject();
    this.loading$ = this.store.pipe(select(selectUserSettingsLoading));
    this.store.pipe(
      select(selectCurrentUser),
      takeUntil(this.componentDestroyed$)
    ).subscribe((user: any) => {
      if (user) {
        this.userModel = _.cloneDeep(user);
        if (this.userModel.payment === undefined) {
          this.userModel.payment = {
            paypal: {
              email: '',
              phoneNumber: ''
            }
          };
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  onCancelUpdate() {
    this.router.navigate(['/dashboard/settings/payment']);
  }

  onSave() {
    this.store.dispatch(new UpdatePayment({ data: _.cloneDeep(this.userModel) }));
  }

  onEdit() {
    this.showEditPaypal = true;
  }


}
