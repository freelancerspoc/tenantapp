export interface UpdateUserSettingModel {
    id: string;
    avatar: string;
    coverPhoto: string;
    firstName: string;
    lastName: string;
    location: string;
    bio: string;
    angelList: string;
    facebook: string;
    twitter: string;
    linkedIn: string;
    website: string;
    hideMyProfile: boolean;
}
