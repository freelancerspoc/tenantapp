export interface User {
    id: string;
    address1: string;
    address2: string;
    avatar: string;
    city: string;
    country: string;
    email: string;
    emailVerified: boolean;
    firstName: string;
    lastName: string;
    password: string;
    payment: Payment;
    phoneNumber: string;
    phoneVerified: boolean;
    postalCode: string;
    state: string;
    street: string;
    username: string;
}

export interface Payment {
    paypal: Paypal;
}

export interface Paypal {
    email: string;
    phoneNumber: string;
}
