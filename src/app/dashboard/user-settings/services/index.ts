import { UserSettingsService } from './user-settings.service';

export {
    UserSettingsService
};

export const SERVICES = [
    UserSettingsService
];
