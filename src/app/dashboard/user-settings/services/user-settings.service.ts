import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UpdateUserSettingModel, ChangePasswordModel, ChangeEmailModel } from '../models';
import { LocalStorageService } from '@app/core/services';
import { environment as env } from '@app/env';

@Injectable()
export class UserSettingsService {

  constructor(private http: HttpClient, private storage: LocalStorageService) { }

  updateProfile(data: UpdateUserSettingModel): Observable<any> {
    return this.http.patch(`/account`, data);
  }

  getCurrentUser(): Observable<any> {
    return this.http.get(`/account`);
  }

  changePassword(data: ChangePasswordModel): Observable<any> {
    return this.http.put(`/account/change-password`, data);
  }

  changeEmail(data: ChangeEmailModel): Observable<any> {
    return this.http.put(`/account/change-email`, data);
  }

  uploadPhoto(formData: FormData): Observable<any> {
    return this.http.post(`/files/upload`, formData);
  }
}
