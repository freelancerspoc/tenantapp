import { Action } from '@ngrx/store';
import { ChangeEmailModel, ChangePasswordModel } from '../../models';

export enum UserSettingsActionTypes {

  UpdateProfileSettings = '[User Settings] Update profile settings',
  UpdateProfileSettingsSuccess = '[User Settings] Update profile settings success',
  UpdateProfileSettingsFailure = '[User Settings] Update profile settings failure',

  ChangeEmail = '[User Settings] Change email',
  ChangeEmailSuccess = '[User Settings] Change email success',
  ChangeEmailFailure = '[User Settings] Change email failure',

  ChangePassword = '[User Settings] Change password',
  ChangePasswordSuccess = '[User Settings] Change password success',
  ChangePasswordFailure = '[User Settings] Change password failure',

  UploadPhoto = '[User Settings] Upload photo',
  UploadPhotoSuccess = '[User Settings] Upload photo success',
  UploadPhotoFailure = '[User Settings] Upload photofailure',

  UpdatePayment = '[User Settings] Update payment',
  UpdatePaymentSuccess = '[User Settings] Update payment success',
  UpdatePaymentFailure = '[User Settings] Update payment failure',
}

export class UpdateProfileSettings implements Action {
  readonly type = UserSettingsActionTypes.UpdateProfileSettings;

  constructor(public payload: { user: any }) { }
}

export class UpdateProfileSettingsSuccess implements Action {
  readonly type = UserSettingsActionTypes.UpdateProfileSettingsSuccess;
  constructor(public payload: { user: any }) { }
}

export class UpdateProfileSettingsFailure implements Action {
  readonly type = UserSettingsActionTypes.UpdateProfileSettingsFailure;

  constructor(public payload: { error: any }) { }
}

export class ChangeEmail implements Action {
  readonly type = UserSettingsActionTypes.ChangeEmail;

  constructor(public payload: ChangeEmailModel) { }
}

export class ChangeEmailSuccess implements Action {
  readonly type = UserSettingsActionTypes.ChangeEmailSuccess;

  constructor(public payload: { email: string, password: any }) { }
}

export class ChangeEmailFailure implements Action {
  readonly type = UserSettingsActionTypes.ChangeEmailFailure;

  constructor(public payload: { error: any }) { }
}


export class ChangePassword implements Action {
  readonly type = UserSettingsActionTypes.ChangePassword;

  constructor(public payload: ChangePasswordModel) { }
}

export class ChangePasswordSuccess implements Action {
  readonly type = UserSettingsActionTypes.ChangePasswordSuccess;
}

export class ChangePasswordFailure implements Action {
  readonly type = UserSettingsActionTypes.ChangePasswordFailure;

  constructor(public payload: { error: any }) { }
}

export class UploadPhoto implements Action {
  readonly type = UserSettingsActionTypes.UploadPhoto;

  constructor(public payload: { formData: FormData }) { }
}

export class UploadPhotoSuccess implements Action {
  readonly type = UserSettingsActionTypes.UploadPhotoSuccess;

  constructor(public payload: { mediaLink: string }) { }
}

export class UploadPhotoFailure implements Action {
  readonly type = UserSettingsActionTypes.UploadPhotoFailure;

  constructor(public payload: { error: any }) { }
}

export class UpdatePayment implements Action {
  readonly type = UserSettingsActionTypes.UpdatePayment;

  constructor(public payload: { data: any }) { }
}

export class UpdatePaymentSuccess implements Action {
  readonly type = UserSettingsActionTypes.UpdatePaymentSuccess;

  constructor(public payload: { data: any }) { }
}

export class UpdatePaymentFailure implements Action {
  readonly type = UserSettingsActionTypes.UpdatePaymentFailure;

  constructor(public payload: { error: any }) { }
}

export type UserSettingsActions =
  UpdateProfileSettings | UpdateProfileSettingsSuccess | UpdateProfileSettingsFailure |
  ChangeEmail | ChangeEmailSuccess | ChangeEmailFailure |
  ChangePassword | ChangePasswordSuccess | ChangePasswordFailure |
  UpdatePayment | UpdatePaymentSuccess | UpdatePaymentFailure |
  UploadPhoto | UploadPhotoSuccess | UploadPhotoFailure;
