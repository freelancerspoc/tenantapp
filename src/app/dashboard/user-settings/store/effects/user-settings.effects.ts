import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap, catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import * as _ from 'lodash';
import { NotificationService } from '@app/ui/notification';
import { UserSettingsService } from '../../services';
import {
  UpdateProfileSettings,
  UserSettingsActionTypes,
  UpdateProfileSettingsSuccess,
  UpdateProfileSettingsFailure,
  ChangeEmail,
  ChangeEmailSuccess,
  ChangeEmailFailure,
  ChangePassword,
  ChangePasswordSuccess,
  ChangePasswordFailure,
  UploadPhoto,
  UploadPhotoSuccess,
  UploadPhotoFailure,
  UpdatePayment,
  UpdatePaymentSuccess,
  UpdatePaymentFailure
} from '../actions/user-settings.actions';
import { GetCurrentUserSuccess, UpdateCurrentUserToLocal } from '@app/auth';
import { environment as env } from '@app/env';

@Injectable()
export class UserSettingsEffects {

  @Effect()
  updateUserSettings$ = this.actions$.pipe(
    ofType<UpdateProfileSettings>(UserSettingsActionTypes.UpdateProfileSettings),
    map(action => action.payload.user),
    switchMap((user) =>
      this.userSettingsService.updateProfile(user).pipe(
        map(() => {
          this.notification.success('Update user successfully.');
          return new UpdateProfileSettingsSuccess({ user: user });
        }),
        catchError(error => {
          this.notification.error('Update user fail.');
          return of(new UpdateProfileSettingsFailure({ error }));
        })
      )
    )
  );

  @Effect()
  updateUserSettingsSuccess$ = this.actions$.pipe(
    ofType<UpdateProfileSettings>(UserSettingsActionTypes.UpdateProfileSettingsSuccess),
    map((action: any) => {
      return new UpdateCurrentUserToLocal({ userProfile: action.payload.user });
    }),

  );

  @Effect()
  changeEmail$ = this.actions$.pipe(
    ofType<ChangeEmail>(UserSettingsActionTypes.ChangeEmail),
    map(action => action.payload),
    switchMap((payload) =>
      this.userSettingsService.changeEmail(payload).pipe(
        map(() => {
          this.notification.success('Change email successfully.');
          return new ChangeEmailSuccess({ email: payload.email, password: payload.password });
        }),
        catchError(error => {
          this.notification.error('Change email fail.');
          return of(new ChangeEmailFailure({ error }));
        })
      )
    )
  );

  @Effect()
  changeEmailSuccess$ = this.actions$.pipe(
    ofType<ChangeEmailSuccess>(UserSettingsActionTypes.ChangeEmailSuccess),
    map((action) => {
      return new UpdateCurrentUserToLocal({ userProfile: { email: action.payload.email, password: action.payload.password } });
    })

  );

  @Effect()
  changePassword$ = this.actions$.pipe(
    ofType<ChangePassword>(UserSettingsActionTypes.ChangePassword),
    map(action => action),
    switchMap((action) =>
      this.userSettingsService.changePassword(action.payload).pipe(
        map(() => {
          this.notification.success('Change password successfully.');
          return new ChangePasswordSuccess();
        }),
        catchError(error => {
          this.notification.error('Change password fail.');
          return of(new ChangePasswordFailure({ error }));
        })
      )
    )
  );

  @Effect()
  uploadPhoto$ = this.actions$.pipe(
    ofType<UploadPhoto>(UserSettingsActionTypes.UploadPhoto),
    map(action => action.payload),
    switchMap((data) =>
      this.userSettingsService.uploadPhoto(data.formData).pipe(
        map((res) => {
          this.notification.success('Upload avatar successfully.');
          const imageUrl = env.imageUrl + res.reference;
          return new UploadPhotoSuccess({ mediaLink: imageUrl });
        }),
        catchError(error => {
          this.notification.error('Upload avatar fail.');
          return of(new UploadPhotoFailure({ error }));
        })
      )
    )
  );


  @Effect()
  uploadPhotoSuccess$ = this.actions$.pipe(
    ofType<UploadPhotoSuccess>(UserSettingsActionTypes.UploadPhotoSuccess),
    map((action: any) => {
      return new UpdateProfileSettings({
        user: {
          avatar: action.payload.mediaLink
        }
      });
    }),
  );

  @Effect()
  updatePayment$ = this.actions$.pipe(
    ofType<UpdatePayment>(UserSettingsActionTypes.UpdatePayment),
    map(action => action.payload.data),
    switchMap((data) =>
      this.userSettingsService.updateProfile(data).pipe(
        map(() => {
          this.notification.success('Update payment successfully.');
          return new UpdatePaymentSuccess({ data: data });
        }),
        catchError(error => {
          this.notification.error('Update payment fail.');
          return of(new UpdatePaymentFailure({ error }));
        })
      )
    )
  );

  @Effect()
  updatePaymentSuccess$ = this.actions$.pipe(
    ofType<UpdatePayment>(UserSettingsActionTypes.UpdatePaymentSuccess),
    map((action: any) => {
      return new GetCurrentUserSuccess({ userProfile: action.payload.data });
    })

  );

  constructor(
    private actions$: Actions,
    private notification: NotificationService,
    private userSettingsService: UserSettingsService
  ) { }
}
