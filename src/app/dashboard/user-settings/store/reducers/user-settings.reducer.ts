import { UserSettingsActions, UserSettingsActionTypes } from '../actions/user-settings.actions';
import { createFeatureSelector } from '@ngrx/store';
import * as _ from 'lodash';

export interface UserSettingsState {
  mediaLink: any | null;
  loadedPhoto: boolean;
  loadingPhoto: boolean;
  loaded: boolean;
  loading: boolean;
  isUpdatePayment: boolean;
  isChangePassword: boolean;
}

export const initialState: UserSettingsState = {
  mediaLink: null,
  loaded: false,
  loading: false,
  loadedPhoto: false,
  loadingPhoto: false,
  isUpdatePayment: false,
  isChangePassword: false
};

export function userSettingsReducer(state = initialState, action: UserSettingsActions): UserSettingsState {
  switch (action.type) {

    case UserSettingsActionTypes.UpdateProfileSettings: {
      return {
        ...state,
        loaded: false,
        loading: true
      };
    }

    case UserSettingsActionTypes.UpdateProfileSettingsSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false
      };
    }

    case UserSettingsActionTypes.UpdateProfileSettingsFailure: {
      return {
        ...state,
        loaded: false,
        loading: false
      };
    }

    case UserSettingsActionTypes.ChangeEmail: {
      return {
        ...state,
        loaded: false,
        loading: true
      };
    }

    case UserSettingsActionTypes.ChangeEmailSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false
      };
    }

    case UserSettingsActionTypes.ChangeEmailFailure: {
      return {
        ...state,
        loaded: false,
        loading: false
      };
    }

    case UserSettingsActionTypes.ChangePassword: {
      return {
        ...state,
        loaded: false,
        loading: true,
        isChangePassword: false
      };
    }

    case UserSettingsActionTypes.ChangePasswordSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false,
        isChangePassword: true
      };
    }

    case UserSettingsActionTypes.ChangePasswordFailure: {
      return {
        ...state,
        loaded: false,
        loading: false,
        isChangePassword: false
      };
    }

    case UserSettingsActionTypes.UploadPhoto: {
      return {
        ...state,
        loadedPhoto: false,
        loadingPhoto: true
      };
    }

    case UserSettingsActionTypes.UploadPhotoSuccess: {
      return {
        ...state,
        loadedPhoto: true,
        loadingPhoto: false
      };
    }

    case UserSettingsActionTypes.UploadPhotoFailure: {
      return {
        ...state,
        loadedPhoto: false,
        loadingPhoto: false
      };
    }

    case UserSettingsActionTypes.UpdatePayment: {
      return {
        ...state,
        loaded: false,
        loading: true
      };
    }

    case UserSettingsActionTypes.UpdatePaymentSuccess: {
      return {
        ...state,
        loaded: true,
        loading: false,
        isUpdatePayment: true
      };
    }

    case UserSettingsActionTypes.UpdatePaymentFailure: {
      return {
        ...state,
        loaded: false,
        loading: false
      };
    }

    default: {
      return state;
    }

  }
}

export const selectFeature = createFeatureSelector<UserSettingsState>('user-settings');
export const getUserSettingsLoading = (state: UserSettingsState) => state.loading;
export const getUserSettingsLoaded = (state: UserSettingsState) => state.loaded;
export const getIsUpdatePayment = (state: UserSettingsState) => state.isUpdatePayment;
export const getPhotoLoading = (state: UserSettingsState) => state.loadingPhoto;
export const getIsChangePassword = (state: UserSettingsState) => state.isChangePassword;


