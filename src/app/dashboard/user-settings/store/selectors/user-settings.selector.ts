import { createSelector } from '@ngrx/store';

import * as fromReducer from '../reducers/user-settings.reducer';


export const selectUserSettingsLoading = createSelector(
    fromReducer.selectFeature,
    fromReducer.getUserSettingsLoading
);

export const selectUserSettingsLoaded = createSelector(
    fromReducer.selectFeature,
    fromReducer.getUserSettingsLoaded
);

export const selectIsUpdatePayment = createSelector(
    fromReducer.selectFeature,
    fromReducer.getIsUpdatePayment
);

export const selectPhotoLoading = createSelector(
    fromReducer.selectFeature,
    fromReducer.getPhotoLoading
);

export const selectIsChangePassword = createSelector(
    fromReducer.selectFeature,
    fromReducer.getIsChangePassword
);

