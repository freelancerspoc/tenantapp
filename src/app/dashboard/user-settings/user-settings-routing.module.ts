import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  SettingsPageComponent,
  ChangePasswordPageComponent,
  ChangeEmailPageComponent,
  PaymentPageComponent,
  SettingProfilePageComponent
} from './containers';

const routes: Routes = [
  {
    path: '',
    component: SettingsPageComponent,
    children: [
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full'
      },
      {
        path: 'change-password',
        component: ChangePasswordPageComponent,
        data: {
          title: 'Change password'
        }
      },
      {
        path: 'change-email',
        component: ChangeEmailPageComponent,
        data: {
          title: 'Change email'
        }
      },
      {
        path: 'payment',
        component: PaymentPageComponent,
        data: {
          title: 'Payment'
        }
      },
      {
        path: 'profile',
        component: SettingProfilePageComponent,
        data: {
          title: 'Profile setting'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserSettingsRoutingModule { }
