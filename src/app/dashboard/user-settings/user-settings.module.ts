import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';
import { ImageCropperModule } from 'ngx-image-cropper';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NgxPayPalModule } from 'ngx-paypal';

import { NotificationModule } from '@app/ui/notification';
import { UserSettingsRoutingModule } from './user-settings-routing.module';
import { UserSettingsEffects } from './store/effects/user-settings.effects';
import * as fromUserSettings from './store/reducers/user-settings.reducer';
import { CONTAINERS } from './containers';
import { COMPONENTS } from './components';
import { SERVICES } from './services';

@NgModule({
  declarations: [
    ...CONTAINERS,
    ...COMPONENTS
  ],
  imports: [
    CommonModule,
    FormsModule,
    FileUploadModule,
    ImageCropperModule,
    NgxPayPalModule,
    InternationalPhoneNumberModule,
    NotificationModule,
    UserSettingsRoutingModule,
    StoreModule.forFeature('user-settings', fromUserSettings.userSettingsReducer),
    EffectsModule.forFeature([UserSettingsEffects])
  ],
  providers: [
    ...SERVICES
  ]
})
export class UserSettingsModule { }
