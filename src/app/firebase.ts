import * as firebase from 'firebase/app';
import 'firebase/auth';
import { environment as env } from '@app/env';

firebase.initializeApp(env.firebaseConfig);

export default firebase;
