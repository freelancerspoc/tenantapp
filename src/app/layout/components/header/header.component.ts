import { Component, OnInit, HostListener, ViewChild, ElementRef, Renderer2 } from '@angular/core';

import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @ViewChild('navScrollTo') navScrollTo: ElementRef;
  showMenu = false;
  url: any;
  verticalOffset = 0;
  @HostListener('window:scroll', []) onWindowScroll() {
    const verticalOffset = window.pageYOffset
      || document.documentElement.scrollTop
      || document.body.scrollTop || 0;
    this.verticalOffset = verticalOffset;
    this.setVerticalOffset();
  }

  constructor(private router: Router, private render: Renderer2) {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((x: NavigationEnd) => {
      if (x.url) {
        this.url = x.url;
      }
      this.setVerticalOffset();
    });
  }

  ngOnInit() {
    this.router.events.subscribe(s => {
      if (s instanceof NavigationEnd) {
        const tree = this.router.parseUrl(this.router.url);
        if (tree.fragment) {
          const element = document.querySelector('#' + tree.fragment);
          if (element) { element.scrollIntoView(); }
        }
      }
    });
  }

  onClickMenu() {
    this.showMenu = !this.showMenu;
  }

  onClickCloseMenu() {
    this.showMenu = false;
  }

  setVerticalOffset() {
    if ((this.verticalOffset === 0) && (this.url === '/')) {
      this.render.removeClass(this.navScrollTo.nativeElement, 'navbar-light');
      this.render.addClass(this.navScrollTo.nativeElement, 'navbar-light-scroll');
    } else if (this.url !== '/') {
      this.render.addClass(this.navScrollTo.nativeElement, 'navbar-light');
      this.render.removeClass(this.navScrollTo.nativeElement, 'navbar-light-scroll');
    } else if ((this.verticalOffset !== 0) && (this.url === '/')) {
      this.render.addClass(this.navScrollTo.nativeElement, 'navbar-light');
      this.render.removeClass(this.navScrollTo.nativeElement, 'navbar-light-scroll');
    }
  }
}
