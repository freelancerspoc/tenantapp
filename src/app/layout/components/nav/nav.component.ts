import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
  animations: [trigger('openClose', [
    state('open', style({
      opacity: 1
    })),
    state('closed', style({
      opacity: 0.5
    })),
    transition('open => closed', animate('1000ms ease-out')),
    transition('closed => open', animate('1000ms ease-in'))
  ])
  ]
})
export class NavComponent implements OnInit {
  @Input() isOpen: any;
  isCollapsedDashboard = false;
  isCollapsedGraph = false;
  isCollapsedMailbox = false;
  isCollapsedForm = false;
  isCollapsedSpecial = false;
  isCollapsedOtherPage = false;
  isCollapsedMiscellaneous = false;
  isCollapsedElement = false;
  isCollapsedTable = false;
  isCollapsedEcommerce = false;
  isCollapsedGallery = false;
  isCollapsedMenuLevel = false;
  isCollapsedThirdLevel = false;
  isOpenProfile = false;
  breadcrumbLink: any;
  constructor() {

  }

  ngOnInit() {
  }
}
