import { Component, OnInit, Renderer2, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, map, mergeMap, takeUntil } from 'rxjs/operators';

import { AuthService } from '@app/auth';
import { select, Store } from '@ngrx/store';
import { AppState } from '@app/core/store';

import * as _ from 'lodash';

import { GetCurrentUser, Logout } from 'src/app/auth/store/actions/auth.actions';
import { selectCurrentUser } from 'src/app/auth/store/selectors/auth.selector';

@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.scss'],
})
export class DashboardLayoutComponent implements OnInit, OnDestroy {

  userModel: any = {};
  breadcrumb: any;
  isOpenProfile = false;
  isCloseMenuNav = true;
  urlImage: any;
  url: any;
  private componentDestroyed$: Subject<boolean>;

  constructor(private renderer: Renderer2, private authService: AuthService,
    private route: ActivatedRoute, private router: Router, private store: Store<AppState>) {
    this.componentDestroyed$ = new Subject();
    this.store.dispatch(new GetCurrentUser);
    this.subscribeNavigationEnd();

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((x: NavigationEnd) => {
      if (x.url) {
        this.url = x.url;
        console.log(this.url);
      }
    });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  ngOnInit() {
    this.store.pipe(
      select(selectCurrentUser),
      takeUntil(this.componentDestroyed$)
    ).subscribe((user: any) => {
      if (user) {
        this.userModel = _.cloneDeep(user);
        this.urlImage = this.userModel.avatar ? this.userModel.avatar : './assets/img/avatar-default.png';
      }
    });
  }

  subscribeNavigationEnd() {
    this.router
      .events.pipe(
      filter(e => e instanceof NavigationEnd))
      .pipe(map(() => this.route))
      .pipe(map(() => {
        let route = this.route.firstChild;
        let child = route;

        while (child) {
          if (child.firstChild) {
            child = child.firstChild;
            route = child;
          } else {
            child = null;
          }
        }

        return route;
      }))
      .pipe(mergeMap(route => route.data))
      .subscribe(e => this.breadcrumb = e ? e : null);
  }

  fnMinimiseMenu() {
    this.isCloseMenuNav = !this.isCloseMenuNav;
    if (this.isCloseMenuNav) {
      this.renderer.removeClass(document.body, 'mini-navbar');
    } else {
      this.renderer.addClass(document.body, 'mini-navbar');
    }
  }

  onClickedOutside(e: Event) {
    this.isOpenProfile = false;
  }

  onLogout() {
    this.store.dispatch(new Logout());
  }
}
