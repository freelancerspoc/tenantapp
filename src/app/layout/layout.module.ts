import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MainLayoutComponent } from './main-layout/main-layout.component';
import { HeaderComponent } from './components/header/header.component';
// import { FooterComponent } from './components/footer/footer.component';
import { DashboardLayoutComponent } from './dashboard-layout/dashboard-layout.component';
import { NavComponent } from './components/nav/nav.component';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';

import { ClickOutsideModule } from 'ng-click-outside';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        NgbCollapseModule,
        ClickOutsideModule
    ],
    declarations: [
        MainLayoutComponent,
        HeaderComponent,
        // FooterComponent,
        DashboardLayoutComponent,
        NavComponent,
    ],
    exports: [
        MainLayoutComponent,
        DashboardLayoutComponent
    ]
})
export class LayoutModule { }
