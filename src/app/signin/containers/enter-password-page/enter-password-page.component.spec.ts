import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterPasswordPageComponent } from './enter-password-page.component';

describe('EnterPasswordPageComponent', () => {
  let component: EnterPasswordPageComponent;
  let fixture: ComponentFixture<EnterPasswordPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterPasswordPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterPasswordPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
