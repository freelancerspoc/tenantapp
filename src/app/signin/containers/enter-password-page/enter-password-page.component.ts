import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as _ from 'lodash';

import {
  AuthState, Login, Credential, selectCheckUser,
  selectLoginPagePending, CheckExistingUser, selectCheckingUserExist,
  CreateUserModel, CreateUser
} from '@app/auth';

@Component({
  selector: 'app-enter-password-page',
  templateUrl: './enter-password-page.component.html',
  styleUrls: ['./enter-password-page.component.scss']
})
export class EnterPasswordPageComponent implements OnInit, OnDestroy {
  loginModel: Credential = {
    username: '',
    password: ''
  };
  userModel: CreateUserModel = {
    username: '',
    password: ''
  };

  checkUser$: Observable<any>;
  loading$: Observable<boolean>;
  private componentDestroyed$: Subject<boolean>;

  constructor(
    private store: Store<AuthState>,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.componentDestroyed$ = new Subject();

    this.loading$ = this.store.pipe(select(selectLoginPagePending));
    this.checkUser$ = this.store.pipe(select(selectCheckingUserExist));

    this.activatedRoute.queryParams.subscribe(params => {
      if (!_.isEmpty(params) && params['phoneNumber']) {
        this.userModel.email = params['email'];
        this.userModel.phoneNumber = ('+' + params['phoneNumber']).replace(/ +/g, '');
        this.userModel.username = ('+' + params['phoneNumber']).replace(/ +/g, '');
        this.userModel.password = 'Onboard2019';
        this.checkExisting(this.userModel.username);

      }
    });

    this.store.pipe(
      select(selectCheckUser),
      takeUntil(this.componentDestroyed$))
      .subscribe(user => {
        switch (user) {
          case undefined:
            if (!this.userModel.username) {
              this.router.navigate(['/signin/identifier']);
            }
            break;
          case null:
            if (this.userModel.username) {
              this.createNewUser(this.userModel);
              this.loginModel.username = this.userModel.username;
            }
            break;
          default:
            this.loginModel.username = user.username;
        }
      });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  onSubmit() {
    this.store.dispatch(new Login({ credential: { ...this.loginModel } }));
  }

  createNewUser(userModel) {
    this.store.dispatch(new CreateUser({ user: userModel }));
  }

  checkExisting(username) {
    this.store.dispatch(new CheckExistingUser({ username: username }));
  }
}
