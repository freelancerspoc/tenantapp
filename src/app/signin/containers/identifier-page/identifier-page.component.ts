import { Component, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { takeUntil, map, skip } from 'rxjs/operators';

import {
  AuthState, CheckExistingUser, selectCheckUser, selectCheckingUserExist
} from '@app/auth';

@Component({
  selector: 'app-identifier-page',
  templateUrl: './identifier-page.component.html',
  styleUrls: ['./identifier-page.component.scss']
})
export class IdentifierPageComponent implements OnDestroy {

  checkParams = null;
  username = '';
  showMessage = false;
  existingUser$: Observable<boolean>;
  checking$: Observable<boolean>;

  private componentDestroyed$: Subject<boolean>;

  constructor(
    private store: Store<AuthState>,
    private router: Router,
  ) {
    this.componentDestroyed$ = new Subject();
    this.checking$ = this.store.pipe(select(selectCheckingUserExist));
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  onSubmit() {
    this.store.dispatch(new CheckExistingUser({ username: this.username }));

    this.store.pipe(
      select(selectCheckUser),
      skip(1),
      takeUntil(this.componentDestroyed$),
      map(user => !!user)
    ).subscribe(exist => {
      if (exist) {
        this.showMessage = false;
        this.router.navigate(['/signin/pwd']);
      } else {
        this.showMessage = true;
      }
    });
  }

}
