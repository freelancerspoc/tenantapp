import { IdentifierPageComponent } from './identifier-page/identifier-page.component';
import { EnterPasswordPageComponent } from './enter-password-page/enter-password-page.component';

export {
  IdentifierPageComponent,
  EnterPasswordPageComponent
};

export const CONTAINERS = [
  IdentifierPageComponent,
  EnterPasswordPageComponent
];
