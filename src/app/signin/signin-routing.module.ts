import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IdentifierPageComponent, EnterPasswordPageComponent } from './containers';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'identifier',
    pathMatch: 'full'
  },
  {
    path: 'identifier',
    component: IdentifierPageComponent
  },
  {
    path: 'pwd',
    component: EnterPasswordPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SigninRoutingModule { }
