import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';

import { SigninRoutingModule } from './signin-routing.module';
import { IdentifierPageComponent } from './containers/identifier-page/identifier-page.component';
import { EnterPasswordPageComponent } from './containers/enter-password-page/enter-password-page.component';

@NgModule({
  declarations: [IdentifierPageComponent, EnterPasswordPageComponent],
  imports: [
    CommonModule,
    FormsModule,
    InternationalPhoneNumberModule,
    SigninRoutingModule
  ]
})
export class SigninModule { }
