import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LocalStorageService } from '@app/core/services';
import { Country } from '../../models/country.model';
import { AccountService } from 'src/app/auth/services/account.service';


@Component({
  selector: 'app-get-started-page',
  templateUrl: './get-started-page.component.html',
  styleUrls: ['./get-started-page.component.scss']
})
export class GetStartedPageComponent implements OnInit {

  userModel: any = {};
  loading = false;
  phone_number_standard = '';
  phone_number_preffered = '';
  preferredCountries: Country[];
  constructor(
    private accountService: AccountService,
    private router: Router,
    private storage: LocalStorageService
  ) {}

  onSubmit() {
    this.loading = true;
    this.accountService
      .checkExisting(this.userModel.phoneNumber)
      .subscribe(res => {
        this.loading = false;
        if (res && res.uid) {
          this.router.navigate(['/login']);
        } else {
          this.createNewAccount();
        }
      });
  }

  createNewAccount(): void {
    this.storage.setItem('userData', this.userModel);
    this.router.navigate(['/signup/verify']);
  }

  ngOnInit() {
  }

}
