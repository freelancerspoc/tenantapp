import { SignupSuccessfulPageComponent } from './signup-successful-page/signup-successful-page.component';
import { GetStartedPageComponent } from './get-started-page/get-started-page.component';
import { PersonalDetailPageComponent } from './personal-detail-page/personal-detail-page.component';
import { VerificationPageComponent } from './verification-page/verification-page.component';
import { SetUsernamePasswordPageComponent } from './set-username-password-page/set-username-password-page.component';
import { SurveyPageComponent } from './survey-page/survey-page.component';
export {
    SignupSuccessfulPageComponent,
    GetStartedPageComponent,
    PersonalDetailPageComponent,
    VerificationPageComponent,
    SetUsernamePasswordPageComponent,
    SurveyPageComponent
};

export const CONTAINERS = [
    SignupSuccessfulPageComponent,
    GetStartedPageComponent,
    PersonalDetailPageComponent,
    VerificationPageComponent,
    SetUsernamePasswordPageComponent,
    SurveyPageComponent
];
