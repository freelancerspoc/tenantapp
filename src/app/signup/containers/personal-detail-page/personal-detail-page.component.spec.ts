import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalDetailPageComponent } from './personal-detail-page.component';

describe('PersonalDetailPageComponent', () => {
  let component: PersonalDetailPageComponent;
  let fixture: ComponentFixture<PersonalDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
