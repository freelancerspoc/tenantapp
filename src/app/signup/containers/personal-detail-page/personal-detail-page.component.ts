import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LocalStorageService } from '@app/core/services';
import { CountriesList } from '../../models/country.model';
import { AccountService } from 'src/app/auth/services/account.service';

@Component({
  selector: 'app-personal-detail-page',
  templateUrl: './personal-detail-page.component.html',
  styleUrls: ['./personal-detail-page.component.scss']
})
export class PersonalDetailPageComponent implements OnInit {

  userModel: any = {};
  countryName = '';
  loading = false;
  countriesList = CountriesList;

  constructor(private storage: LocalStorageService, private getStartedService: AccountService, private router: Router) { }

  ngOnInit() {
    this.userModel = this.storage.getItem('userData');
  }

  onSubmit() {
    this.loading = true;
    this.userModel.country = this.countryName;
    this.getStartedService.updateTenant(this.userModel).subscribe(res => {
      this.loading = false;
      if (res.success) {
        this.router.navigate(['/signup/update']);
      }
    });
  }

}
