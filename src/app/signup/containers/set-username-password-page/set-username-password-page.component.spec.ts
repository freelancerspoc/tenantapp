import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetUsernamePasswordPageComponent } from './set-username-password-page.component';

describe('SetUsernamePasswordPageComponent', () => {
  let component: SetUsernamePasswordPageComponent;
  let fixture: ComponentFixture<SetUsernamePasswordPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetUsernamePasswordPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetUsernamePasswordPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
