import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from '@app/core/services';
import { AccountService } from '@app/auth';

@Component({
  selector: 'app-set-username-password-page',
  templateUrl: './set-username-password-page.component.html',
  styleUrls: ['./set-username-password-page.component.scss']
})
export class SetUsernamePasswordPageComponent implements OnInit {

  userModel: any = {};
  loading = false;

  constructor(private storage: LocalStorageService, private accountService: AccountService, private router: Router) { }

  ngOnInit() {
    this.userModel = this.storage.getItem('userData');
  }

  onSubmit() {
    this.loading = true;
    this.storage.setItem('userData', this.userModel);
    this.accountService.setUseranmeAndPassword(this.userModel).subscribe(res => {
      this.loading = false;
      if (res.success) {
        this.router.navigate(['/signup/survey']);
      }
    });
  }

}
