import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup-successful-page',
  templateUrl: './signup-successful-page.component.html',
  styleUrls: ['./signup-successful-page.component.scss']
})
export class SignupSuccessfulPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
