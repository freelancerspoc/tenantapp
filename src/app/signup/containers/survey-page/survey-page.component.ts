import { Component, OnInit } from '@angular/core';
import * as Survey from 'survey-angular';
import { LocalStorageService } from '@app/core/services';
import * as _ from 'lodash';
import { SurveyService, AuthService, AuthState, AuthToken } from '@app/auth';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
Survey.JsonObject.metaData.addProperty('questionbase', 'popupdescription:text');
Survey.JsonObject.metaData.addProperty('page', 'popupdescription:text');

@Component({
  selector: 'app-survey-page',
  templateUrl: './survey-page.component.html',
  styleUrls: ['./survey-page.component.scss']
})
export class SurveyPageComponent implements OnInit {

  userModel: any = {};
  isLoading = false;

  constructor(private surveyService: SurveyService, private store: Store<AuthState>,
    private storage: LocalStorageService, private authService: AuthService,
    private router: Router) {
    this.initializeSurvey();
  }

  ngOnInit() {
    this.userModel = this.storage.getItem('userData');
  }
  initializeSurvey() {
    const surveyCss = {
      root: 'sv_main',
      navigationButton: 'btn btn-green',
      body: 'sv_body',
      navigation: {
        complete: 'btn sv_complete_btn btn-primary btn-lg  mt-4',
        prev: 'btn sv_prev_btn btn-primary btn-lg  mt-4',
        next: 'btn sv_next_btn btn-primary btn-lg  mt-4',
        start: 'btn sv_start_btn btn-primary btn-lg  mt-4'
      }
    };

    this.surveyService.getQuestions().subscribe((res: any) => {
      if (!res) {
        return;
      }
      const model = {
        pages: []
      };
      res.forEach((data) => {
        model.pages.push({
          questions: [data]
        });
      });

      const surveyModel = new Survey.Model(model);

      surveyModel.onComplete.add(result => {
        this.onSurveyComplete(result.data);
      });

      Survey.SurveyNG.render('surveyElement', { model: surveyModel, css: surveyCss });
    });
  }

  onSurveyComplete(result) {
    this.isLoading = true;
    this.surveyService.saveSurvey(this.userModel.uid, result).subscribe(res => {
      this.authService.login({username: this.userModel.username, password: this.userModel.password}).subscribe((response: any) => {
        const tokenModel = new AuthToken(response.accessToken, response.expiresIn);
        this.authService.saveAuthToken(tokenModel);
        this.router.navigate(['/dashboard']);
        this.isLoading = false;
      });
    });
  }

}
