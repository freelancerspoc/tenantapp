import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { zip } from 'rxjs';

import firebase from '../../../firebase';
import { LocalStorageService } from '@app/core/services';
import { NotificationService } from '@app/ui/notification';
import { AccountService, CreateDefaultAccountModel, UpdateUserAuthenticationModel } from '@app/auth';

@Component({
  selector: 'app-verification-page',
  templateUrl: './verification-page.component.html',
  styleUrls: ['./verification-page.component.scss']
})
export class VerificationPageComponent implements OnInit {
  userModel: any;
  sendCode = false;
  verifyField = false;
  resetBtn = false;
  verificationCode = '';
  captchaVisible = true;
  loading = false;
  loadingRecaptcha = true;
  recaptchaIsVerified = false;

  private recaptchaVerifier = null;
  private confirmationResult = null;

  constructor(
    private ngZone: NgZone,
    private router: Router,
    private notification: NotificationService,
    private accountService: AccountService,
    private storage: LocalStorageService
  ) { }

  ngOnInit() {
    this.userModel = this.storage.getItem('userData');
    if (!this.userModel) {
      this.router.navigate(['/']);
    }
    this.createReCaptcha();
  }

  createReCaptcha() {
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

    this.recaptchaVerifier
      .render()
      .then(() => {
        this.hideLoadingRecaptchaIndicator();
      })
      .catch((error) => {
        console.log(error);
      });

    this.recaptchaVerifier
      .verify()
      .then(() => {
        this.sendVerificationCode();
      })
      .catch((error) => {
        console.log('verify captcha error', error);
      });
  }

  resetCaptcha() {
    this.resetBtn = false;
    this.verifyField = false;
    this.captchaVisible = true;
    this.recaptchaVerifier.reset();
    this.recaptchaVerifier
      .verify()
      .then(() => {
        this.sendVerificationCode();
      });
  }

  hideLoadingRecaptchaIndicator() {
    this.ngZone.run(() => {
      this.loadingRecaptcha = false;
    });
  }

  sendCodeView() {
    this.ngZone.run(() => {
      this.verifyField = true;
      this.sendCode = false;
      this.resetBtn = true;
      this.captchaVisible = false;
    });
  }

  sendVerificationCode() {
    this.loading = true;
    firebase
      .auth()
      .signInWithPhoneNumber(this.userModel.phoneNumber, this.recaptchaVerifier)
      .then(result => {
        this.loading = false;
        this.confirmationResult = result;
        this.sendCodeView();
      })
      .catch(error => {
        console.log('send verification code error: ', error);
      });
  }

  verifyCode() {
    this.loading = true;

    this.confirmationResult
      .confirm(this.verificationCode)
      .then((result) => {
        this.userModel.uid = result.user.uid;
        this.storage.setItem('userData', this.userModel);
        this.createAccount(result.user.uid);
      })
      .catch(error => {
        this.loading = false;
        this.notification.error('The verification code is wrong.');
      });
  }

  createAccount(uid: string) {
    const createDefaultAccountModel: CreateDefaultAccountModel = {
      uid: uid,
      firstName: this.userModel.firstName,
      lastName: this.userModel.lastName,
      email: this.userModel.email,
      phoneNumber: this.userModel.phoneNumber
    };

    const updateUserAuthenticationModel: UpdateUserAuthenticationModel = {
      uid: uid,
      displayName: this.userModel.firstName + ' ' + this.userModel.lastName,
      email: this.userModel.email,
      phoneNumber: this.userModel.phoneNumber
    };

    zip(
      this.accountService.createDefaultAccount(createDefaultAccountModel),
      this.accountService.updateUserAuthentication(updateUserAuthenticationModel)
    ).subscribe(() => {
      this.loading = false;
      this.router.navigate(['/signup/personal-detail']);
    });
  }

}
