import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  SignupSuccessfulPageComponent,
  GetStartedPageComponent,
  PersonalDetailPageComponent,
  VerificationPageComponent,
  SurveyPageComponent,
  SetUsernamePasswordPageComponent
} from './containers';

const routes: Routes = [
  {
    path: 'success',
    component: SignupSuccessfulPageComponent
  },
  {
    path: 'get-started',
    component: GetStartedPageComponent
  },
  {
    path: 'personal-detail',
    component: PersonalDetailPageComponent
  },
  {
    path: 'verify',
    component: VerificationPageComponent
  },
  {
    path: 'survey',
    component: SurveyPageComponent
  },
  {
    path: 'update',
    component: SetUsernamePasswordPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignupRoutingModule { }
