import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';

import { SignupRoutingModule } from './signup-routing.module';
import { CONTAINERS } from './containers';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    InternationalPhoneNumberModule,
    SignupRoutingModule
  ],

  declarations: [
    ...CONTAINERS
  ]
})
export class SignupModule { }
