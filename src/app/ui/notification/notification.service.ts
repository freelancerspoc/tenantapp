import { Injectable } from '@angular/core';

import * as Notyf from 'notyf';

@Injectable()
export class NotificationService {

  private notyf: any;

  constructor() {
    this.notyf = new Notyf({
      delay: 4000,
      alertIcon: '',
      warnIcon: '',
      confirmIcon: ''
    });
  }

  success(message: string): void {
    this.notyf.confirm(message);
  }

  error(message: string): void {
    this.notyf.alert(message);
  }
}
