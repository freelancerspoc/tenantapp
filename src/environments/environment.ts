export const environment = {
  production: false,
  appName: 'Onboard web app',
  rootApi: 'https://us-central1-kari-karthick.cloudfunctions.net',
  maxRetryAttempts: 2,
  autoAuthorizedUris: [
    new RegExp('/account')
  ],
  sanboxClientId: 'AZ6YxjTfEZNz_tB17WBnpIW-pvJ0ydZTH0E_xkCGB_Agi5JXC3oKpdWg9mUWXb_gJ-SfIAM0nUo265JQ',
  authTokenKey: 'AUTHENTICATION_TOKEN',
  loggedInUserKey: 'LOGGEDIN_USER',
  imageUrl: 'https://storage.googleapis.com/kari-karthick.appspot.com/',
  firebaseConfig: {
    apiKey: "AIzaSyC2n--OT-7n3C9a5jmGAYxzOXWmDtvEF3Q",
    authDomain: "kari-karthick.firebaseapp.com",
    databaseURL: "https://kari-karthick.firebaseio.com",
    projectId: "kari-karthick",
    storageBucket: "kari-karthick.appspot.com",
    messagingSenderId: "1091534635909",
    appId: "1:1091534635909:web:bb5fce7d11d8700e"
  }
};